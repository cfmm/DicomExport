#!/usr/bin/env bash
set -x

platform=${1}

root_dir=$(pwd)

edm_path=`which edm`
if [[ $? == 1 ]]; then
  if [[ "$platform" == "win64" ]]; then
      edm_url="https://package-data.enthought.com/edm/win_x86_64/2.5/edm_cli_2.5.0_x86_64.msi"
      curl -o "$(basename ${edm_url})" "${edm_url}"
      msiexec -i $(basename ${edm_url}) -qn -log log.log
      value=$(<log.log)
      echo "$value"
      edm_path="${HOME}/AppData/Local/Programs/Enthought/edm/edm.bat"
  else
      if [[ `uname` == 'Darwin' ]]; then
        edm_url="https://package-data.enthought.com/edm/osx_x86_64/2.5/edm_cli_2.5.0_osx_x86_64.sh"
      else
        edm_url="https://package-data.enthought.com/edm/rh6_x86_64/2.5/edm_cli_2.5.0_linux_x86_64.sh"
      fi
      wget "${edm_url}"
      bash "$(basename ${edm_url})" -b --bypass-bundle-install
      edm_path="$(pwd)/edm/bin/edm"
  fi
fi

git submodule update --init --recursive

${edm_path} install -y --environment DicomExportBuild --version 3.6 pip wheel
env_prefix="${edm_path} run --environment DicomExportBuild --"
mkdir -p pip_wheels
mkdir -p eggs
cd pip_wheels
${env_prefix} pip download --no-binary=:all: -r "../requirements.txt"

for f in $(ls *.tar.gz); do tar xvf ${f}; done
for p in $(ls -d */); do
    ${env_prefix} pip wheel --wheel-dir=${p} ${p};
    ${env_prefix} edm repack-wheel --environment DicomExportBuild ${p}/*.whl;
    cp "${p}"/*.egg "${root_dir}/eggs"
    #edm egginst -e DicomExportBuild $p/*.egg;
done
cd ..
for p in pynetdicom python-zipstream DicomRaw .; do
    cd ${p}
    ${env_prefix} python setup.py bdist_wheel
    ${env_prefix} edm repack-wheel --environment DicomExportBuild dist/*.whl
    cp dist/*.egg "${root_dir}/eggs"
    #edm egginst dist/*.egg
    cd -
done
${edm_path} install -y -e DicomExport --version 3.6
${edm_path} environments export --include-assets --output-file DicomExport.databundle DicomExport
