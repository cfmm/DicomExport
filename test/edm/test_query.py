import unittest

from DicomExport import Query
from test.test_scp import LocalSCP

Console_3T = {
    "remote_host": "prisma.imaging.robarts.ca",
    "remote_port": 104,
    "remote_ae_title": "MRC35368"
}


class TestStudyQuery(unittest.TestCase):

    def setUp(self):
        self.server = LocalSCP()
        self.server.run()
        config = self.server.config
        self.querySCU = Query(config['host'], config['port'], config['ae_title'], 'TestStudyQuery')
        # self.querySCU = Query(**Console_3T)

    def tearDown(self):
        self.querySCU.close()
        self.server.stop()

    def test_query_for_studies(self):
        response = self.querySCU.query_studies()
        studies = [study.StudyInstanceUID for study in response]
        self.assertTrue('1' in studies)
        self.assertTrue('2' in studies)
        self.assertTrue('3' in studies)
        self.assertTrue('4' in studies)

    def test_query_for_study_details(self):
        study_uuid = '1'
        response = self.querySCU.query_studies(StudyInstanceUID=study_uuid, StudyDescription='')
        study_details = next(response)
        self.assertEqual(study_details.StudyDescription, 'Test Study {}'.format(study_uuid))
        with self.assertRaises(StopIteration):
            next(response)
