import os
import unittest
from tempfile import NamedTemporaryFile

from DicomExport import Store
from DicomExport import RawDicomDataset
from DicomExport.main import get_scu

from test.test_scp import LocalSCP

Console_3T = {
    "query_server": {"host": "prisma.imaging.robarts.ca",
                     "port": 104,
                     "ae_title": "MRC35368"},
    "store_server": {"host": "dcm4chee.local",
                     "port": 11114,
                     "ae_title": "CFMM-Public"},
    "client_ae_title": "DEFAULT"
}


class TestRawDicomDataset(unittest.TestCase):

    def setUp(self):
        self.server = LocalSCP()
        self.server.run()
        self.query = get_scu(self.server.config, "Querier")
        self.store = get_scu(self.server.config, "Storer", Store)
        # self.query = get_scu(Console_3T["query_server"], Console_3T["client_ae_title"])
        # self.store = get_scu(Console_3T["store_server"], Console_3T["client_ae_title"], Store)
        self.raw_dataset = RawDicomDataset(query=self.query, store=self.store)
        response = self.raw_dataset._query.query_studies(StudyDescription='')
        self.first_study = next(response)
        # pynetdicom expects you to handle all returned values, or cancel the query
        while True:
            try:
                next(response)
            except StopIteration:
                break

    def tearDown(self):
        self.query.close()
        self.store.close()
        self.server.stop()

    def test_dicom_send(self):
        with NamedTemporaryFile(dir=os.curdir, delete=False) as rand_input_file:
            rand_input_file.write(os.urandom(1000000))
            rand_input_file.flush()
            filename = rand_input_file.name

        self.raw_dataset.append(
            input_paths=[filename],
            study_instance_uid=self.first_study.StudyInstanceUID,
            series_description="Random test file"
        )

        try:
            os.remove(filename)
        except OSError:
            pass

        # Check correct StudyInstanceUID in received data
        self.assertEqual(self.server.data.StudyInstanceUID, self.first_study.StudyInstanceUID)
