-- noinspection SpellCheckingInspectionForFile

\out /dev/null

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

-- noinspection SqlResolve
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: animalpositionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE animalpositionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    orientation character varying(255)
);


ALTER TABLE public.animalpositionentity OWNER TO sa;

--
-- Name: animalsubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE animalsubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL,
    subjectdate timestamp without time zone,
    uniqueid character varying(255),
    dateofdelivery date,
    displayname character varying(255),
    species integer,
    gender character varying(255)
);


ALTER TABLE public.animalsubjectentity OWNER TO sa;

--
-- Name: contrastagentinstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE contrastagentinstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    duration bigint NOT NULL,
    index integer NOT NULL,
    status character varying(255),
    statusmessage text,
    userinputapplied boolean NOT NULL,
    userinputneeded boolean NOT NULL,
    userstartneeded boolean NOT NULL,
    scanprogramentity_id bigint,
    percentcompleted double precision NOT NULL,
    postagentpause bigint NOT NULL,
    preagentpause bigint NOT NULL
);


ALTER TABLE public.contrastagentinstructionentity OWNER TO sa;

--
-- Name: copyreferenceentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE copyreferenceentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    parametergroupid character varying(255),
    destinationinstructionentity_id bigint,
    sourceinstructionentity_id bigint
);


ALTER TABLE public.copyreferenceentity OWNER TO sa;

--
-- Name: copyreferenceproxyentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE copyreferenceproxyentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    destinationstoragelocation character varying(434),
    parametergroupid character varying(255),
    referenceid bigint,
    sourcestoragelocation character varying(434)
);


ALTER TABLE public.copyreferenceproxyentity OWNER TO sa;

--
-- Name: datasetentity_archivedescriptors; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_archivedescriptors (
    datasetentity_id bigint NOT NULL,
    comment text,
    date timestamp without time zone,
    location character varying(255)
);


ALTER TABLE public.datasetentity_archivedescriptors OWNER TO sa;

--
-- Name: datasetentity_booleanarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_booleanarrayproperties (
    datasetentity_id bigint NOT NULL,
    booleanarrayproperties bytea,
    booleanarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_booleanarrayproperties OWNER TO sa;

--
-- Name: datasetentity_booleanproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_booleanproperties (
    datasetentity_id bigint NOT NULL,
    booleanproperties boolean,
    booleanproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_booleanproperties OWNER TO sa;

--
-- Name: datasetentity_bytearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_bytearrayproperties (
    datasetentity_id bigint NOT NULL,
    bytearrayproperties bytea,
    bytearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_bytearrayproperties OWNER TO sa;

--
-- Name: datasetentity_byteproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_byteproperties (
    datasetentity_id bigint NOT NULL,
    byteproperties smallint,
    byteproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_byteproperties OWNER TO sa;

--
-- Name: datasetentity_doublearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_doublearrayproperties (
    datasetentity_id bigint NOT NULL,
    doublearrayproperties bytea,
    doublearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_doublearrayproperties OWNER TO sa;

--
-- Name: datasetentity_doubleproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_doubleproperties (
    datasetentity_id bigint NOT NULL,
    doubleproperties double precision,
    doubleproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_doubleproperties OWNER TO sa;

--
-- Name: datasetentity_floatarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_floatarrayproperties (
    datasetentity_id bigint NOT NULL,
    floatarrayproperties bytea,
    floatarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_floatarrayproperties OWNER TO sa;

--
-- Name: datasetentity_floatproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_floatproperties (
    datasetentity_id bigint NOT NULL,
    floatproperties real,
    floatproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_floatproperties OWNER TO sa;

--
-- Name: datasetentity_intarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_intarrayproperties (
    datasetentity_id bigint NOT NULL,
    intarrayproperties bytea,
    intarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_intarrayproperties OWNER TO sa;

--
-- Name: datasetentity_intproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_intproperties (
    datasetentity_id bigint NOT NULL,
    intproperties integer,
    intproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_intproperties OWNER TO sa;

--
-- Name: datasetentity_longarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_longarrayproperties (
    datasetentity_id bigint NOT NULL,
    longarrayproperties bytea,
    longarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_longarrayproperties OWNER TO sa;

--
-- Name: datasetentity_longproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_longproperties (
    datasetentity_id bigint NOT NULL,
    longproperties bigint,
    longproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_longproperties OWNER TO sa;

--
-- Name: datasetentity_shortarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_shortarrayproperties (
    datasetentity_id bigint NOT NULL,
    shortarrayproperties bytea,
    shortarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_shortarrayproperties OWNER TO sa;

--
-- Name: datasetentity_shortproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_shortproperties (
    datasetentity_id bigint NOT NULL,
    shortproperties smallint,
    shortproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_shortproperties OWNER TO sa;

--
-- Name: datasetentity_stringarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_stringarrayproperties (
    datasetentity_id bigint NOT NULL,
    stringarrayproperties bytea,
    stringarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetentity_stringarrayproperties OWNER TO sa;

--
-- Name: datasetentity_stringproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetentity_stringproperties (
    datasetentity_id bigint NOT NULL,
    stringproperties text,
    stringproperties_key text NOT NULL
);


ALTER TABLE public.datasetentity_stringproperties OWNER TO sa;

--
-- Name: datasetserviceentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    datasetserviceid character varying(255),
    displayname character varying(255),
    postservicedentity_id bigint,
    preservicedentity_id bigint
);


ALTER TABLE public.datasetserviceentity OWNER TO sa;

--
-- Name: datasetserviceentity_booleanarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_booleanarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    booleanarrayproperties bytea,
    booleanarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_booleanarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_booleanproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_booleanproperties (
    datasetserviceentity_id bigint NOT NULL,
    booleanproperties boolean,
    booleanproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_booleanproperties OWNER TO sa;

--
-- Name: datasetserviceentity_bytearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_bytearrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    bytearrayproperties bytea,
    bytearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_bytearrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_byteproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_byteproperties (
    datasetserviceentity_id bigint NOT NULL,
    byteproperties smallint,
    byteproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_byteproperties OWNER TO sa;

--
-- Name: datasetserviceentity_doublearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_doublearrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    doublearrayproperties bytea,
    doublearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_doublearrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_doubleproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_doubleproperties (
    datasetserviceentity_id bigint NOT NULL,
    doubleproperties double precision,
    doubleproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_doubleproperties OWNER TO sa;

--
-- Name: datasetserviceentity_floatarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_floatarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    floatarrayproperties bytea,
    floatarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_floatarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_floatproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_floatproperties (
    datasetserviceentity_id bigint NOT NULL,
    floatproperties real,
    floatproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_floatproperties OWNER TO sa;

--
-- Name: datasetserviceentity_intarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_intarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    intarrayproperties bytea,
    intarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_intarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_intproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_intproperties (
    datasetserviceentity_id bigint NOT NULL,
    intproperties integer,
    intproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_intproperties OWNER TO sa;

--
-- Name: datasetserviceentity_longarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_longarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    longarrayproperties bytea,
    longarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_longarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_longproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_longproperties (
    datasetserviceentity_id bigint NOT NULL,
    longproperties bigint,
    longproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_longproperties OWNER TO sa;

--
-- Name: datasetserviceentity_shortarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_shortarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    shortarrayproperties bytea,
    shortarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_shortarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_shortproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_shortproperties (
    datasetserviceentity_id bigint NOT NULL,
    shortproperties smallint,
    shortproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_shortproperties OWNER TO sa;

--
-- Name: datasetserviceentity_stringarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_stringarrayproperties (
    datasetserviceentity_id bigint NOT NULL,
    stringarrayproperties bytea,
    stringarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.datasetserviceentity_stringarrayproperties OWNER TO sa;

--
-- Name: datasetserviceentity_stringproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE datasetserviceentity_stringproperties (
    datasetserviceentity_id bigint NOT NULL,
    stringproperties text,
    stringproperties_key text NOT NULL
);


ALTER TABLE public.datasetserviceentity_stringproperties OWNER TO sa;

--
-- Name: examinationentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE examinationentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    acqtime timestamp without time zone,
    dimensiontypes character varying(255),
    displayname character varying(255),
    imageseriesindex integer NOT NULL,
    index integer NOT NULL,
    offline boolean NOT NULL,
    rawdataavailable boolean NOT NULL,
    scaninstructionentity_id bigint,
    studyentity_id bigint
);


ALTER TABLE public.examinationentity OWNER TO sa;

--
-- Name: frequencycategoryentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE frequencycategoryentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    comment text,
    registration character varying(255)
);


ALTER TABLE public.frequencycategoryentity OWNER TO sa;

--
-- Name: fungussubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE fungussubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL,
    subjectdate timestamp without time zone,
    uniqueid character varying(255),
    dateofdelivery date,
    displayname character varying(255),
    species integer
);


ALTER TABLE public.fungussubjectentity OWNER TO sa;

--
-- Name: gradientcategoryentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE gradientcategoryentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    comment text,
    registration character varying(255),
    frequencycategoryentity_id bigint
);


ALTER TABLE public.gradientcategoryentity OWNER TO sa;

--
-- Name: hibernate_sequences; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE hibernate_sequences (
    sequence_name character varying(255),
    sequence_next_hi_value integer
);


ALTER TABLE public.hibernate_sequences OWNER TO sa;

--
-- Name: humanpositionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE humanpositionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    orientation character varying(255)
);


ALTER TABLE public.humanpositionentity OWNER TO sa;

--
-- Name: humansubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE humansubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL,
    subjectdate timestamp without time zone,
    uniqueid character varying(255),
    personentity_id bigint
);


ALTER TABLE public.humansubjectentity OWNER TO sa;

--
-- Name: imageseriesentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE imageseriesentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    dimensiontypes character varying(255),
    framecount integer,
    imagetime timestamp without time zone,
    importtime timestamp without time zone,
    index integer NOT NULL,
    modality character varying(255),
    offline boolean NOT NULL,
    recodataavailable boolean NOT NULL,
    standardvisudescription boolean NOT NULL,
    typeid character varying(255),
    examinationentity_id bigint
);


ALTER TABLE public.imageseriesentity OWNER TO sa;

--
-- Name: logmessageentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE logmessageentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    details text,
    header character varying(255),
    logcategory character varying(255),
    loglevel character varying(255),
    message text,
    "timestamp" bigint NOT NULL,
    title character varying(255)
);


ALTER TABLE public.logmessageentity OWNER TO sa;

--
-- Name: materialsubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE materialsubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL,
    subjectdate timestamp without time zone,
    uniqueid character varying(255),
    age bigint NOT NULL,
    displayname character varying(255)
);


ALTER TABLE public.materialsubjectentity OWNER TO sa;

--
-- Name: operatorentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE operatorentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    personentity_id bigint
);


ALTER TABLE public.operatorentity OWNER TO sa;

--
-- Name: pauseinstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE pauseinstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    duration bigint NOT NULL,
    index integer NOT NULL,
    status character varying(255),
    statusmessage text,
    userinputapplied boolean NOT NULL,
    userinputneeded boolean NOT NULL,
    userstartneeded boolean NOT NULL,
    scanprogramentity_id bigint,
    percentcompleted double precision NOT NULL
);


ALTER TABLE public.pauseinstructionentity OWNER TO sa;

--
-- Name: personentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE personentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    dateofbirth date,
    firstname character varying(255),
    gender character varying(255),
    lastname character varying(255),
    middlename character varying(255),
    title character varying(255)
);


ALTER TABLE public.personentity OWNER TO sa;

--
-- Name: plantsubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE plantsubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL,
    subjectdate timestamp without time zone,
    uniqueid character varying(255),
    dateofdelivery date,
    displayname character varying(255),
    species integer
);


ALTER TABLE public.plantsubjectentity OWNER TO sa;

--
-- Name: processinginstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    openforedit boolean NOT NULL,
    processingserviceid character varying(255),
    processingstatus character varying(255),
    dstimageseriesentity_id bigint
);


ALTER TABLE public.processinginstructionentity OWNER TO sa;

--
-- Name: processinginstructionentity_booleanarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_booleanarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    booleanarrayproperties bytea,
    booleanarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_booleanarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_booleanproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_booleanproperties (
    processinginstructionentity_id bigint NOT NULL,
    booleanproperties boolean,
    booleanproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_booleanproperties OWNER TO sa;

--
-- Name: processinginstructionentity_bytearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_bytearrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    bytearrayproperties bytea,
    bytearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_bytearrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_byteproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_byteproperties (
    processinginstructionentity_id bigint NOT NULL,
    byteproperties smallint,
    byteproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_byteproperties OWNER TO sa;

--
-- Name: processinginstructionentity_doublearrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_doublearrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    doublearrayproperties bytea,
    doublearrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_doublearrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_doubleproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_doubleproperties (
    processinginstructionentity_id bigint NOT NULL,
    doubleproperties double precision,
    doubleproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_doubleproperties OWNER TO sa;

--
-- Name: processinginstructionentity_floatarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_floatarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    floatarrayproperties bytea,
    floatarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_floatarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_floatproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_floatproperties (
    processinginstructionentity_id bigint NOT NULL,
    floatproperties real,
    floatproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_floatproperties OWNER TO sa;

--
-- Name: processinginstructionentity_imageseriesentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_imageseriesentity (
    usingprocessinginstructionentities_id bigint NOT NULL,
    srcimageseriesentities_id bigint NOT NULL
);


ALTER TABLE public.processinginstructionentity_imageseriesentity OWNER TO sa;

--
-- Name: processinginstructionentity_intarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_intarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    intarrayproperties bytea,
    intarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_intarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_intproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_intproperties (
    processinginstructionentity_id bigint NOT NULL,
    intproperties integer,
    intproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_intproperties OWNER TO sa;

--
-- Name: processinginstructionentity_longarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_longarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    longarrayproperties bytea,
    longarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_longarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_longproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_longproperties (
    processinginstructionentity_id bigint NOT NULL,
    longproperties bigint,
    longproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_longproperties OWNER TO sa;

--
-- Name: processinginstructionentity_shortarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_shortarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    shortarrayproperties bytea,
    shortarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_shortarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_shortproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_shortproperties (
    processinginstructionentity_id bigint NOT NULL,
    shortproperties smallint,
    shortproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_shortproperties OWNER TO sa;

--
-- Name: processinginstructionentity_stringarrayproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_stringarrayproperties (
    processinginstructionentity_id bigint NOT NULL,
    stringarrayproperties bytea,
    stringarrayproperties_key character varying(255) NOT NULL
);


ALTER TABLE public.processinginstructionentity_stringarrayproperties OWNER TO sa;

--
-- Name: processinginstructionentity_stringproperties; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE processinginstructionentity_stringproperties (
    processinginstructionentity_id bigint NOT NULL,
    stringproperties text,
    stringproperties_key text NOT NULL
);


ALTER TABLE public.processinginstructionentity_stringproperties OWNER TO sa;

--
-- Name: projectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE projectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    displayname character varying(255),
    registration character varying(255),
    projectleaderentity_id bigint,
    referencesubjectentity_id bigint
);


ALTER TABLE public.projectentity OWNER TO sa;

--
-- Name: projectentity_subjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE projectentity_subjectentity (
    projectentity_id bigint NOT NULL,
    subjectentities_id bigint NOT NULL
);


ALTER TABLE public.projectentity_subjectentity OWNER TO sa;

--
-- Name: projectleaderentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE projectleaderentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    personentity_id bigint
);


ALTER TABLE public.projectleaderentity OWNER TO sa;

--
-- Name: referencesubjectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE referencesubjectentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    registration character varying(255),
    sessionindex integer NOT NULL
);


ALTER TABLE public.referencesubjectentity OWNER TO sa;

--
-- Name: repositorymethodentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositorymethodentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    description text,
    displayname character varying(255),
    region character varying(255),
    specimen character varying(255),
    archiveddate timestamp without time zone,
    bruker boolean NOT NULL,
    deleteprotection boolean NOT NULL,
    forallfrequencies boolean NOT NULL,
    forallgradientsystems boolean NOT NULL,
    nonservice boolean NOT NULL,
    service boolean NOT NULL,
    storedtime timestamp without time zone,
    frequencycategoryentity_id bigint,
    gradientcategoryentity_id bigint
);


ALTER TABLE public.repositorymethodentity OWNER TO sa;

--
-- Name: repositoryprocessinginstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryprocessinginstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    openforedit boolean NOT NULL,
    processingserviceid character varying(255),
    processingstatus character varying(255),
    dstimageseriesentity_id bigint,
    imageseriesindex integer NOT NULL,
    instructionindex integer NOT NULL,
    examinationlocal boolean NOT NULL,
    studylocal boolean NOT NULL,
    scaninstructionentity_id bigint
);


ALTER TABLE public.repositoryprocessinginstructionentity OWNER TO sa;

--
-- Name: repositoryprocessinginstructionentity_srcimageseriesaddresses; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryprocessinginstructionentity_srcimageseriesaddresses (
    repositoryprocessinginstructionentity_id bigint NOT NULL,
    imageseriesindex integer NOT NULL,
    instructionindex integer NOT NULL
);


ALTER TABLE public.repositoryprocessinginstructionentity_srcimageseriesaddresses OWNER TO sa;

--
-- Name: repositoryprotocolentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryprotocolentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    description text,
    displayname character varying(255),
    region character varying(255),
    specimen character varying(255),
    archiveddate timestamp without time zone,
    bruker boolean NOT NULL,
    deleteprotection boolean NOT NULL,
    forallfrequencies boolean NOT NULL,
    forallgradientsystems boolean NOT NULL,
    nonservice boolean NOT NULL,
    service boolean NOT NULL,
    storedtime timestamp without time zone,
    frequencycategoryentity_id bigint,
    gradientcategoryentity_id bigint,
    cvsdate character varying(255),
    cvsrevision character varying(255),
    method text
);


ALTER TABLE public.repositoryprotocolentity OWNER TO sa;

--
-- Name: repositoryprotocolentity_archivedescriptors; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryprotocolentity_archivedescriptors (
    repositoryprotocolentity_id bigint NOT NULL,
    comment text,
    date timestamp without time zone,
    location character varying(255)
);


ALTER TABLE public.repositoryprotocolentity_archivedescriptors OWNER TO sa;

--
-- Name: repositoryscaninstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryscaninstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    duration bigint NOT NULL,
    index integer NOT NULL,
    status character varying(255),
    statusmessage text,
    userinputapplied boolean NOT NULL,
    userinputneeded boolean NOT NULL,
    userstartneeded boolean NOT NULL,
    scanprogramentity_id bigint,
    adjpercentcompleted double precision NOT NULL,
    individualsetupstepkeysstring text,
    ondemandsetupstepkey character varying(255),
    performprocessings boolean NOT NULL,
    prototypemode boolean NOT NULL,
    recopercentcompleted double precision NOT NULL,
    scanmode character varying(255),
    scanpercentcompleted double precision NOT NULL,
    scaninstructionprotocolentity_id bigint
);


ALTER TABLE public.repositoryscaninstructionentity OWNER TO sa;

--
-- Name: repositoryscanprogramentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryscanprogramentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    displayname character varying(255),
    locked boolean NOT NULL,
    nonservice boolean NOT NULL,
    region character varying(255),
    service boolean NOT NULL,
    specimen character varying(255),
    archiveddate timestamp without time zone,
    bruker boolean NOT NULL,
    comment character varying(255),
    cvsdate character varying(255),
    cvsrevision character varying(255),
    deleteprotection boolean NOT NULL,
    frequencycategoryentity_id bigint,
    gradientcategoryentity_id bigint
);


ALTER TABLE public.repositoryscanprogramentity OWNER TO sa;

--
-- Name: repositoryscanprogramentity_archivedescriptors; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE repositoryscanprogramentity_archivedescriptors (
    repositoryscanprogramentity_id bigint NOT NULL,
    comment text,
    date timestamp without time zone,
    location character varying(255)
);


ALTER TABLE public.repositoryscanprogramentity_archivedescriptors OWNER TO sa;

--
-- Name: scaninstructionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE scaninstructionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    displayname character varying(255),
    duration bigint NOT NULL,
    index integer NOT NULL,
    status character varying(255),
    statusmessage text,
    userinputapplied boolean NOT NULL,
    userinputneeded boolean NOT NULL,
    userstartneeded boolean NOT NULL,
    scanprogramentity_id bigint,
    adjpercentcompleted double precision NOT NULL,
    individualsetupstepkeysstring text,
    ondemandsetupstepkey character varying(255),
    performprocessings boolean NOT NULL,
    prototypemode boolean NOT NULL,
    recopercentcompleted double precision NOT NULL,
    scanmode character varying(255),
    scanpercentcompleted double precision NOT NULL,
    scaninstructionprotocolentity_id bigint
);


ALTER TABLE public.scaninstructionentity OWNER TO sa;

--
-- Name: scaninstructionprotocolentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE scaninstructionprotocolentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    description text,
    displayname character varying(255),
    region character varying(255),
    specimen character varying(255),
    archiveddate timestamp without time zone,
    bruker boolean NOT NULL,
    deleteprotection boolean NOT NULL,
    forallfrequencies boolean NOT NULL,
    forallgradientsystems boolean NOT NULL,
    nonservice boolean NOT NULL,
    service boolean NOT NULL,
    storedtime timestamp without time zone,
    frequencycategoryentity_id bigint,
    gradientcategoryentity_id bigint
);


ALTER TABLE public.scaninstructionprotocolentity OWNER TO sa;

--
-- Name: scaninstructionrepositoryprotocolentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE scaninstructionrepositoryprotocolentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    description text,
    displayname character varying(255),
    region character varying(255),
    specimen character varying(255),
    archiveddate timestamp without time zone,
    bruker boolean NOT NULL,
    deleteprotection boolean NOT NULL,
    forallfrequencies boolean NOT NULL,
    forallgradientsystems boolean NOT NULL,
    nonservice boolean NOT NULL,
    service boolean NOT NULL,
    storedtime timestamp without time zone,
    frequencycategoryentity_id bigint,
    gradientcategoryentity_id bigint,
    method text
);


ALTER TABLE public.scaninstructionrepositoryprotocolentity OWNER TO sa;

--
-- Name: sessionentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE sessionentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    displayname character varying(255),
    index integer NOT NULL,
    studyindex integer NOT NULL,
    projectentity_id bigint,
    subjectentity_id bigint
);


ALTER TABLE public.sessionentity OWNER TO sa;

--
-- Name: studyentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE studyentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    archiveddate timestamp without time zone,
    comment text,
    icon oid,
    openeddate timestamp without time zone,
    storagelocation character varying(434),
    userpropertystring1 character varying(255),
    userpropertystring2 character varying(255),
    active boolean NOT NULL,
    adjustmentconfiguration text,
    closed boolean NOT NULL,
    displayname character varying(255),
    examinationindex integer NOT NULL,
    gradientcoilsystem text,
    index integer NOT NULL,
    location character varying(255),
    offline boolean NOT NULL,
    registration character varying(255),
    rfcoilconfig character varying(255),
    shimcoilsystem text,
    studydate timestamp without time zone,
    subjectweight double precision NOT NULL,
    operatorentity_id bigint,
    positionentity_id bigint,
    sessionentity_id bigint,
    studyscanprogramentity_id bigint
);


ALTER TABLE public.studyentity OWNER TO sa;

--
-- Name: studyscanprogramentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE studyscanprogramentity (
    id bigint NOT NULL,
    creationdate timestamp without time zone,
    favorite boolean NOT NULL,
    modificationdate timestamp without time zone,
    application character varying(255),
    displayname character varying(255),
    locked boolean NOT NULL,
    nonservice boolean NOT NULL,
    region character varying(255),
    service boolean NOT NULL,
    specimen character varying(255)
);


ALTER TABLE public.studyscanprogramentity OWNER TO sa;

--
-- Name: subjectentity_projectentity; Type: TABLE; Schema: public; Owner: sa; Tablespace:
--

CREATE TABLE subjectentity_projectentity (
    subjectentity_id bigint NOT NULL,
    projectentities_id bigint NOT NULL
);


ALTER TABLE public.subjectentity_projectentity OWNER TO sa;


--
-- Name: animalpositionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY animalpositionentity
    ADD CONSTRAINT animalpositionentity_pkey PRIMARY KEY (id);


--
-- Name: animalsubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY animalsubjectentity
    ADD CONSTRAINT animalsubjectentity_pkey PRIMARY KEY (id);


--
-- Name: contrastagentinstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY contrastagentinstructionentity
    ADD CONSTRAINT contrastagentinstructionentity_pkey PRIMARY KEY (id);


--
-- Name: copyreferenceentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY copyreferenceentity
    ADD CONSTRAINT copyreferenceentity_pkey PRIMARY KEY (id);


--
-- Name: copyreferenceproxyentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY copyreferenceproxyentity
    ADD CONSTRAINT copyreferenceproxyentity_pkey PRIMARY KEY (id);


--
-- Name: datasetentity_booleanarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_booleanarrayproperties
    ADD CONSTRAINT datasetentity_booleanarrayproperties_pkey PRIMARY KEY (datasetentity_id, booleanarrayproperties_key);


--
-- Name: datasetentity_booleanproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_booleanproperties
    ADD CONSTRAINT datasetentity_booleanproperties_pkey PRIMARY KEY (datasetentity_id, booleanproperties_key);


--
-- Name: datasetentity_bytearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_bytearrayproperties
    ADD CONSTRAINT datasetentity_bytearrayproperties_pkey PRIMARY KEY (datasetentity_id, bytearrayproperties_key);


--
-- Name: datasetentity_byteproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_byteproperties
    ADD CONSTRAINT datasetentity_byteproperties_pkey PRIMARY KEY (datasetentity_id, byteproperties_key);


--
-- Name: datasetentity_doublearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_doublearrayproperties
    ADD CONSTRAINT datasetentity_doublearrayproperties_pkey PRIMARY KEY (datasetentity_id, doublearrayproperties_key);


--
-- Name: datasetentity_doubleproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_doubleproperties
    ADD CONSTRAINT datasetentity_doubleproperties_pkey PRIMARY KEY (datasetentity_id, doubleproperties_key);


--
-- Name: datasetentity_floatarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_floatarrayproperties
    ADD CONSTRAINT datasetentity_floatarrayproperties_pkey PRIMARY KEY (datasetentity_id, floatarrayproperties_key);


--
-- Name: datasetentity_floatproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_floatproperties
    ADD CONSTRAINT datasetentity_floatproperties_pkey PRIMARY KEY (datasetentity_id, floatproperties_key);


--
-- Name: datasetentity_intarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_intarrayproperties
    ADD CONSTRAINT datasetentity_intarrayproperties_pkey PRIMARY KEY (datasetentity_id, intarrayproperties_key);


--
-- Name: datasetentity_intproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_intproperties
    ADD CONSTRAINT datasetentity_intproperties_pkey PRIMARY KEY (datasetentity_id, intproperties_key);


--
-- Name: datasetentity_longarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_longarrayproperties
    ADD CONSTRAINT datasetentity_longarrayproperties_pkey PRIMARY KEY (datasetentity_id, longarrayproperties_key);


--
-- Name: datasetentity_longproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_longproperties
    ADD CONSTRAINT datasetentity_longproperties_pkey PRIMARY KEY (datasetentity_id, longproperties_key);


--
-- Name: datasetentity_shortarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_shortarrayproperties
    ADD CONSTRAINT datasetentity_shortarrayproperties_pkey PRIMARY KEY (datasetentity_id, shortarrayproperties_key);


--
-- Name: datasetentity_shortproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_shortproperties
    ADD CONSTRAINT datasetentity_shortproperties_pkey PRIMARY KEY (datasetentity_id, shortproperties_key);


--
-- Name: datasetentity_stringarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_stringarrayproperties
    ADD CONSTRAINT datasetentity_stringarrayproperties_pkey PRIMARY KEY (datasetentity_id, stringarrayproperties_key);


--
-- Name: datasetentity_stringproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetentity_stringproperties
    ADD CONSTRAINT datasetentity_stringproperties_pkey PRIMARY KEY (datasetentity_id, stringproperties_key);


--
-- Name: datasetserviceentity_booleanarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_booleanarrayproperties
    ADD CONSTRAINT datasetserviceentity_booleanarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, booleanarrayproperties_key);


--
-- Name: datasetserviceentity_booleanproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_booleanproperties
    ADD CONSTRAINT datasetserviceentity_booleanproperties_pkey PRIMARY KEY (datasetserviceentity_id, booleanproperties_key);


--
-- Name: datasetserviceentity_bytearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_bytearrayproperties
    ADD CONSTRAINT datasetserviceentity_bytearrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, bytearrayproperties_key);


--
-- Name: datasetserviceentity_byteproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_byteproperties
    ADD CONSTRAINT datasetserviceentity_byteproperties_pkey PRIMARY KEY (datasetserviceentity_id, byteproperties_key);


--
-- Name: datasetserviceentity_doublearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_doublearrayproperties
    ADD CONSTRAINT datasetserviceentity_doublearrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, doublearrayproperties_key);


--
-- Name: datasetserviceentity_doubleproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_doubleproperties
    ADD CONSTRAINT datasetserviceentity_doubleproperties_pkey PRIMARY KEY (datasetserviceentity_id, doubleproperties_key);


--
-- Name: datasetserviceentity_floatarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_floatarrayproperties
    ADD CONSTRAINT datasetserviceentity_floatarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, floatarrayproperties_key);


--
-- Name: datasetserviceentity_floatproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_floatproperties
    ADD CONSTRAINT datasetserviceentity_floatproperties_pkey PRIMARY KEY (datasetserviceentity_id, floatproperties_key);


--
-- Name: datasetserviceentity_intarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_intarrayproperties
    ADD CONSTRAINT datasetserviceentity_intarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, intarrayproperties_key);


--
-- Name: datasetserviceentity_intproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_intproperties
    ADD CONSTRAINT datasetserviceentity_intproperties_pkey PRIMARY KEY (datasetserviceentity_id, intproperties_key);


--
-- Name: datasetserviceentity_longarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_longarrayproperties
    ADD CONSTRAINT datasetserviceentity_longarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, longarrayproperties_key);


--
-- Name: datasetserviceentity_longproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_longproperties
    ADD CONSTRAINT datasetserviceentity_longproperties_pkey PRIMARY KEY (datasetserviceentity_id, longproperties_key);


--
-- Name: datasetserviceentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity
    ADD CONSTRAINT datasetserviceentity_pkey PRIMARY KEY (id);


--
-- Name: datasetserviceentity_shortarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_shortarrayproperties
    ADD CONSTRAINT datasetserviceentity_shortarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, shortarrayproperties_key);


--
-- Name: datasetserviceentity_shortproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_shortproperties
    ADD CONSTRAINT datasetserviceentity_shortproperties_pkey PRIMARY KEY (datasetserviceentity_id, shortproperties_key);


--
-- Name: datasetserviceentity_stringarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_stringarrayproperties
    ADD CONSTRAINT datasetserviceentity_stringarrayproperties_pkey PRIMARY KEY (datasetserviceentity_id, stringarrayproperties_key);


--
-- Name: datasetserviceentity_stringproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY datasetserviceentity_stringproperties
    ADD CONSTRAINT datasetserviceentity_stringproperties_pkey PRIMARY KEY (datasetserviceentity_id, stringproperties_key);


--
-- Name: examinationentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY examinationentity
    ADD CONSTRAINT examinationentity_pkey PRIMARY KEY (id);


--
-- Name: frequencycategoryentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY frequencycategoryentity
    ADD CONSTRAINT frequencycategoryentity_pkey PRIMARY KEY (id);


--
-- Name: fungussubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY fungussubjectentity
    ADD CONSTRAINT fungussubjectentity_pkey PRIMARY KEY (id);


--
-- Name: gradientcategoryentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY gradientcategoryentity
    ADD CONSTRAINT gradientcategoryentity_pkey PRIMARY KEY (id);


--
-- Name: humanpositionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY humanpositionentity
    ADD CONSTRAINT humanpositionentity_pkey PRIMARY KEY (id);


--
-- Name: humansubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY humansubjectentity
    ADD CONSTRAINT humansubjectentity_pkey PRIMARY KEY (id);


--
-- Name: imageseriesentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY imageseriesentity
    ADD CONSTRAINT imageseriesentity_pkey PRIMARY KEY (id);


--
-- Name: logmessageentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY logmessageentity
    ADD CONSTRAINT logmessageentity_pkey PRIMARY KEY (id);


--
-- Name: materialsubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY materialsubjectentity
    ADD CONSTRAINT materialsubjectentity_pkey PRIMARY KEY (id);


--
-- Name: operatorentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY operatorentity
    ADD CONSTRAINT operatorentity_pkey PRIMARY KEY (id);


--
-- Name: pauseinstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY pauseinstructionentity
    ADD CONSTRAINT pauseinstructionentity_pkey PRIMARY KEY (id);


--
-- Name: personentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY personentity
    ADD CONSTRAINT personentity_pkey PRIMARY KEY (id);


--
-- Name: plantsubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY plantsubjectentity
    ADD CONSTRAINT plantsubjectentity_pkey PRIMARY KEY (id);


--
-- Name: processinginstructionentity_booleanarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_booleanarrayproperties
    ADD CONSTRAINT processinginstructionentity_booleanarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, booleanarrayproperties_key);


--
-- Name: processinginstructionentity_booleanproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_booleanproperties
    ADD CONSTRAINT processinginstructionentity_booleanproperties_pkey PRIMARY KEY (processinginstructionentity_id, booleanproperties_key);


--
-- Name: processinginstructionentity_bytearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_bytearrayproperties
    ADD CONSTRAINT processinginstructionentity_bytearrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, bytearrayproperties_key);


--
-- Name: processinginstructionentity_byteproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_byteproperties
    ADD CONSTRAINT processinginstructionentity_byteproperties_pkey PRIMARY KEY (processinginstructionentity_id, byteproperties_key);


--
-- Name: processinginstructionentity_doublearrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_doublearrayproperties
    ADD CONSTRAINT processinginstructionentity_doublearrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, doublearrayproperties_key);


--
-- Name: processinginstructionentity_doubleproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_doubleproperties
    ADD CONSTRAINT processinginstructionentity_doubleproperties_pkey PRIMARY KEY (processinginstructionentity_id, doubleproperties_key);


--
-- Name: processinginstructionentity_floatarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_floatarrayproperties
    ADD CONSTRAINT processinginstructionentity_floatarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, floatarrayproperties_key);


--
-- Name: processinginstructionentity_floatproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_floatproperties
    ADD CONSTRAINT processinginstructionentity_floatproperties_pkey PRIMARY KEY (processinginstructionentity_id, floatproperties_key);


--
-- Name: processinginstructionentity_intarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_intarrayproperties
    ADD CONSTRAINT processinginstructionentity_intarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, intarrayproperties_key);


--
-- Name: processinginstructionentity_intproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_intproperties
    ADD CONSTRAINT processinginstructionentity_intproperties_pkey PRIMARY KEY (processinginstructionentity_id, intproperties_key);


--
-- Name: processinginstructionentity_longarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_longarrayproperties
    ADD CONSTRAINT processinginstructionentity_longarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, longarrayproperties_key);


--
-- Name: processinginstructionentity_longproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_longproperties
    ADD CONSTRAINT processinginstructionentity_longproperties_pkey PRIMARY KEY (processinginstructionentity_id, longproperties_key);


--
-- Name: processinginstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity
    ADD CONSTRAINT processinginstructionentity_pkey PRIMARY KEY (id);


--
-- Name: processinginstructionentity_shortarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_shortarrayproperties
    ADD CONSTRAINT processinginstructionentity_shortarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, shortarrayproperties_key);


--
-- Name: processinginstructionentity_shortproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_shortproperties
    ADD CONSTRAINT processinginstructionentity_shortproperties_pkey PRIMARY KEY (processinginstructionentity_id, shortproperties_key);


--
-- Name: processinginstructionentity_stringarrayproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_stringarrayproperties
    ADD CONSTRAINT processinginstructionentity_stringarrayproperties_pkey PRIMARY KEY (processinginstructionentity_id, stringarrayproperties_key);


--
-- Name: processinginstructionentity_stringproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY processinginstructionentity_stringproperties
    ADD CONSTRAINT processinginstructionentity_stringproperties_pkey PRIMARY KEY (processinginstructionentity_id, stringproperties_key);


--
-- Name: projectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY projectentity
    ADD CONSTRAINT projectentity_pkey PRIMARY KEY (id);


--
-- Name: projectleaderentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY projectleaderentity
    ADD CONSTRAINT projectleaderentity_pkey PRIMARY KEY (id);


--
-- Name: referencesubjectentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY referencesubjectentity
    ADD CONSTRAINT referencesubjectentity_pkey PRIMARY KEY (id);


--
-- Name: repositorymethodentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY repositorymethodentity
    ADD CONSTRAINT repositorymethodentity_pkey PRIMARY KEY (id);


--
-- Name: repositoryprocessinginstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY repositoryprocessinginstructionentity
    ADD CONSTRAINT repositoryprocessinginstructionentity_pkey PRIMARY KEY (id);


--
-- Name: repositoryprotocolentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY repositoryprotocolentity
    ADD CONSTRAINT repositoryprotocolentity_pkey PRIMARY KEY (id);


--
-- Name: repositoryscaninstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY repositoryscaninstructionentity
    ADD CONSTRAINT repositoryscaninstructionentity_pkey PRIMARY KEY (id);


--
-- Name: repositoryscanprogramentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY repositoryscanprogramentity
    ADD CONSTRAINT repositoryscanprogramentity_pkey PRIMARY KEY (id);


--
-- Name: scaninstructionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY scaninstructionentity
    ADD CONSTRAINT scaninstructionentity_pkey PRIMARY KEY (id);


--
-- Name: scaninstructionprotocolentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY scaninstructionprotocolentity
    ADD CONSTRAINT scaninstructionprotocolentity_pkey PRIMARY KEY (id);


--
-- Name: scaninstructionrepositoryprotocolentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY scaninstructionrepositoryprotocolentity
    ADD CONSTRAINT scaninstructionrepositoryprotocolentity_pkey PRIMARY KEY (id);


--
-- Name: sessionentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY sessionentity
    ADD CONSTRAINT sessionentity_pkey PRIMARY KEY (id);


--
-- Name: studyentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY studyentity
    ADD CONSTRAINT studyentity_pkey PRIMARY KEY (id);


--
-- Name: studyscanprogramentity_pkey; Type: CONSTRAINT; Schema: public; Owner: sa; Tablespace:
--

ALTER TABLE ONLY studyscanprogramentity
    ADD CONSTRAINT studyscanprogramentity_pkey PRIMARY KEY (id);


--
-- Name: fk_18o02cki14r7b4clnr5y1tu0x; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_floatarrayproperties
    ADD CONSTRAINT fk_18o02cki14r7b4clnr5y1tu0x FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_1a2umuh0lp0vc0pfv3gnwmp3x; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY scaninstructionprotocolentity
    ADD CONSTRAINT fk_1a2umuh0lp0vc0pfv3gnwmp3x FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: fk_31kaihqnfhgx9nehql88dlh71; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_shortproperties
    ADD CONSTRAINT fk_31kaihqnfhgx9nehql88dlh71 FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_42ylvobflwon0ht7acn7xuwio; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY subjectentity_projectentity
    ADD CONSTRAINT fk_42ylvobflwon0ht7acn7xuwio FOREIGN KEY (projectentities_id) REFERENCES projectentity(id);


--
-- Name: fk_51ppwi7rknp0m0i26brbcn74n; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryprotocolentity
    ADD CONSTRAINT fk_51ppwi7rknp0m0i26brbcn74n FOREIGN KEY (gradientcategoryentity_id) REFERENCES gradientcategoryentity(id);


--
-- Name: fk_5dqbno75v0892qf2mjduy10fk; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryprocessinginstructionentity
    ADD CONSTRAINT fk_5dqbno75v0892qf2mjduy10fk FOREIGN KEY (dstimageseriesentity_id) REFERENCES imageseriesentity(id);


--
-- Name: fk_6jsqstbgj9w4wu7gtu1803h75; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryprotocolentity_archivedescriptors
    ADD CONSTRAINT fk_6jsqstbgj9w4wu7gtu1803h75 FOREIGN KEY (repositoryprotocolentity_id) REFERENCES repositoryprotocolentity(id);


--
-- Name: fk_7cvguyq8wft0eo4bwgsdyy0qq; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY studyentity
    ADD CONSTRAINT fk_7cvguyq8wft0eo4bwgsdyy0qq FOREIGN KEY (studyscanprogramentity_id) REFERENCES studyscanprogramentity(id);


--
-- Name: fk_7mptmsdrxl317wle6nwgmeqsm; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_longarrayproperties
    ADD CONSTRAINT fk_7mptmsdrxl317wle6nwgmeqsm FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_7nw6i1gjg63ea8y0pchudln8b; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_shortarrayproperties
    ADD CONSTRAINT fk_7nw6i1gjg63ea8y0pchudln8b FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_8ng1era12dpjmub1mr2dycoqr; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryprocessinginstructionentity_srcimageseriesaddresses
    ADD CONSTRAINT fk_8ng1era12dpjmub1mr2dycoqr FOREIGN KEY (repositoryprocessinginstructionentity_id) REFERENCES repositoryprocessinginstructionentity(id);


--
-- Name: fk_8tcg7xhrnr8fehror14qeiha8; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY operatorentity
    ADD CONSTRAINT fk_8tcg7xhrnr8fehror14qeiha8 FOREIGN KEY (personentity_id) REFERENCES personentity(id);


--
-- Name: fk_95qfpvgt9fyl8n2474ntrrn8i; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_booleanarrayproperties
    ADD CONSTRAINT fk_95qfpvgt9fyl8n2474ntrrn8i FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_9j1bixwu3i8crsjawib28hmx1; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY gradientcategoryentity
    ADD CONSTRAINT fk_9j1bixwu3i8crsjawib28hmx1 FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: fk_a60t9swmlh55tjym907gm1rtx; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY studyentity
    ADD CONSTRAINT fk_a60t9swmlh55tjym907gm1rtx FOREIGN KEY (positionentity_id) REFERENCES humanpositionentity(id);


--
-- Name: fk_aqtib2p9389g218aqx4h7b41d; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY scaninstructionprotocolentity
    ADD CONSTRAINT fk_aqtib2p9389g218aqx4h7b41d FOREIGN KEY (gradientcategoryentity_id) REFERENCES gradientcategoryentity(id);


--
-- Name: fk_av5myi39g7rfhssfg9td883b1; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_intarrayproperties
    ADD CONSTRAINT fk_av5myi39g7rfhssfg9td883b1 FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_bb9klqd7lvf7jvgxosds3afib; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY imageseriesentity
    ADD CONSTRAINT fk_bb9klqd7lvf7jvgxosds3afib FOREIGN KEY (examinationentity_id) REFERENCES examinationentity(id);


--
-- Name: fk_beq358bpcugvrlla6mg1njf2i; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY sessionentity
    ADD CONSTRAINT fk_beq358bpcugvrlla6mg1njf2i FOREIGN KEY (projectentity_id) REFERENCES projectentity(id);


--
-- Name: fk_bosrh2kx5ccp2l1915cp53v16; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_booleanproperties
    ADD CONSTRAINT fk_bosrh2kx5ccp2l1915cp53v16 FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_cdlhc1txw4x4l3bvk0d79a7tr; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_doublearrayproperties
    ADD CONSTRAINT fk_cdlhc1txw4x4l3bvk0d79a7tr FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_dch6b1ri6t5lb8oktkr8yo22m; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_stringarrayproperties
    ADD CONSTRAINT fk_dch6b1ri6t5lb8oktkr8yo22m FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_f89miui1p0uaoar0rbxa5lttw; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY projectleaderentity
    ADD CONSTRAINT fk_f89miui1p0uaoar0rbxa5lttw FOREIGN KEY (personentity_id) REFERENCES personentity(id);


--
-- Name: fk_fgb0a63e07gpbagx371edy2ln; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_bytearrayproperties
    ADD CONSTRAINT fk_fgb0a63e07gpbagx371edy2ln FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_fmw4x0lsetpiabk3ji3m47boc; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY projectentity
    ADD CONSTRAINT fk_fmw4x0lsetpiabk3ji3m47boc FOREIGN KEY (referencesubjectentity_id) REFERENCES referencesubjectentity(id);


--
-- Name: fk_get35ht593efc8oyg9yrx0dfa; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_doubleproperties
    ADD CONSTRAINT fk_get35ht593efc8oyg9yrx0dfa FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_gyo92qebuw93tw0ts498fujv3; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_longproperties
    ADD CONSTRAINT fk_gyo92qebuw93tw0ts498fujv3 FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_hca2kkh1pqj1unpnppq8ual6w; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_intproperties
    ADD CONSTRAINT fk_hca2kkh1pqj1unpnppq8ual6w FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_hjtiy890h6dy9bmwsk6sx2x01; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY scaninstructionrepositoryprotocolentity
    ADD CONSTRAINT fk_hjtiy890h6dy9bmwsk6sx2x01 FOREIGN KEY (gradientcategoryentity_id) REFERENCES gradientcategoryentity(id);


--
-- Name: fk_hwgqyakem2y096ywqgm00xote; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY studyentity
    ADD CONSTRAINT fk_hwgqyakem2y096ywqgm00xote FOREIGN KEY (operatorentity_id) REFERENCES operatorentity(id);


--
-- Name: fk_j85g0bt8pbga495rgcnhkc17e; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_stringproperties
    ADD CONSTRAINT fk_j85g0bt8pbga495rgcnhkc17e FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_jaibeish8vqtferh8v6pesi75; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY processinginstructionentity
    ADD CONSTRAINT fk_jaibeish8vqtferh8v6pesi75 FOREIGN KEY (dstimageseriesentity_id) REFERENCES imageseriesentity(id);


--
-- Name: fk_k8abwvnv1yfr9lvl0m7i2vfpd; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryscanprogramentity_archivedescriptors
    ADD CONSTRAINT fk_k8abwvnv1yfr9lvl0m7i2vfpd FOREIGN KEY (repositoryscanprogramentity_id) REFERENCES repositoryscanprogramentity(id);


--
-- Name: fk_m19qf95ihorxxmet90iff3b3f; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY humansubjectentity
    ADD CONSTRAINT fk_m19qf95ihorxxmet90iff3b3f FOREIGN KEY (personentity_id) REFERENCES personentity(id);


--
-- Name: fk_meu51ukt47g4qbfsxfb1hpk25; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositorymethodentity
    ADD CONSTRAINT fk_meu51ukt47g4qbfsxfb1hpk25 FOREIGN KEY (gradientcategoryentity_id) REFERENCES gradientcategoryentity(id);


--
-- Name: fk_mo2lj64dap92g8sr9frfgfjfb; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_floatproperties
    ADD CONSTRAINT fk_mo2lj64dap92g8sr9frfgfjfb FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_mypvyrco6kjw5gdstjwfni76c; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY projectentity
    ADD CONSTRAINT fk_mypvyrco6kjw5gdstjwfni76c FOREIGN KEY (projectleaderentity_id) REFERENCES projectleaderentity(id);


--
-- Name: fk_nxpko00r97ggin1arw0ov8nmd; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY projectentity_subjectentity
    ADD CONSTRAINT fk_nxpko00r97ggin1arw0ov8nmd FOREIGN KEY (projectentity_id) REFERENCES projectentity(id);


--
-- Name: fk_p604fawajd2encm991bd6klrg; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositorymethodentity
    ADD CONSTRAINT fk_p604fawajd2encm991bd6klrg FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: fk_py61pvhot74ydrfn6afaxj2al; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryscanprogramentity
    ADD CONSTRAINT fk_py61pvhot74ydrfn6afaxj2al FOREIGN KEY (gradientcategoryentity_id) REFERENCES gradientcategoryentity(id);


--
-- Name: fk_qassk2fsrgtfh7152gocf0632; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryprotocolentity
    ADD CONSTRAINT fk_qassk2fsrgtfh7152gocf0632 FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: fk_rfqk9bmdibwkwly92lq0ctdb8; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY processinginstructionentity_imageseriesentity
    ADD CONSTRAINT fk_rfqk9bmdibwkwly92lq0ctdb8 FOREIGN KEY (srcimageseriesentities_id) REFERENCES imageseriesentity(id);


--
-- Name: fk_rnvaux6rvr9n8jdfkc9u7rj3c; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY studyentity
    ADD CONSTRAINT fk_rnvaux6rvr9n8jdfkc9u7rj3c FOREIGN KEY (sessionentity_id) REFERENCES sessionentity(id);


--
-- Name: fk_shpe2beubbdhbbkyu3cxxx55h; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY scaninstructionrepositoryprotocolentity
    ADD CONSTRAINT fk_shpe2beubbdhbbkyu3cxxx55h FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: fk_t1t004880whkqitqp7t178vkc; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY datasetserviceentity_byteproperties
    ADD CONSTRAINT fk_t1t004880whkqitqp7t178vkc FOREIGN KEY (datasetserviceentity_id) REFERENCES datasetserviceentity(id);


--
-- Name: fk_tn54s27h9b6t75duu2w50k0o3; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY examinationentity
    ADD CONSTRAINT fk_tn54s27h9b6t75duu2w50k0o3 FOREIGN KEY (studyentity_id) REFERENCES studyentity(id);


--
-- Name: fk_tpgngd542007pa5757n3g6lur; Type: FK CONSTRAINT; Schema: public; Owner: sa
--

ALTER TABLE ONLY repositoryscanprogramentity
    ADD CONSTRAINT fk_tpgngd542007pa5757n3g6lur FOREIGN KEY (frequencycategoryentity_id) REFERENCES frequencycategoryentity(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: nmr
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nmr;
GRANT ALL ON SCHEMA public TO nmr;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

