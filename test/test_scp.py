import random
from threading import Thread

from pydicom._storage_sopclass_uids import EnhancedMRImageStorage, MRImageStorage
from pydicom.dataset import Dataset, Tag


from DicomExport.store import RawImageSOPClassUID
from pynetdicom import AE, evt

HOST = 'localhost'
DEST_AE_TITLE = 'TEST_AE_TITLE'


class LocalSCP(Thread):

    def __init__(self):
        self.ae = AE(ae_title=DEST_AE_TITLE)
        self.ae.add_supported_context(EnhancedMRImageStorage)
        self.ae.add_supported_context(MRImageStorage)
        # Verification SOP Class
        self.ae.add_supported_context('1.2.840.10008.1.1')
        # Study Root Query/Retrieve Information Model - FIND
        self.ae.add_supported_context('1.2.840.10008.5.1.4.1.2.2.1')
        self.ae.add_supported_context(RawImageSOPClassUID)

        self.server = None
        self.data = Dataset()
        super().__init__()

    @property
    def port(self):
        return self.server.socket.getsockname()[1]

    @property
    def host(self):
        return 'localhost'

    def run(self):
        handlers = [(evt.EVT_C_FIND, self.handle_find),
                    (evt.EVT_C_ECHO, self.handle_echo),
                    (evt.EVT_C_STORE, self.handle_store)]
        self.server = self.ae.start_server(address=('localhost', random.randint(1028, 65535)),
                                           block=False,
                                           evt_handlers=handlers)

    def stop(self):
        self.server.shutdown()
        self.ae.shutdown()

    @property
    def config(self):
        return {'host': self.host,
                'port': self.port,
                'ae_title': self.ae.ae_title}

    @staticmethod
    def handle_echo(event):
        return 0x0000

    def handle_store(self, event):
        self.data = event.dataset
        self.data.file_meta = event.file_meta
        return 0x0000

    @staticmethod
    def handle_find(event):
        ds = event.identifier

        if 'QueryRetrieveLevel' not in ds:
            # Failure
            yield 0xC000, None
            return

        # Create a list of dummy instances
        instances = list()
        for i in range(1, 5):
            instance = Dataset()
            instance.PatientID = '{}'.format(i)
            instance.PatientName = 'Test Patient {}'.format(i)
            instance.StudyDate = '20200101'
            instance.StudyTime = '000000.0'
            instance.AccessionNumber = ''
            instance.StudyID = '{}'.format(i)
            instance.StudyDescription = 'Test Study {}'.format(i)
            instance.StudyInstanceUID = '{}'.format(i)

            instance.PatientSex = 'O'
            instance.PatientBirthDate = ''
            instance.AccessionNumber = ''
            instance.ReferringPhysicianName = ''

            instances.append(instance)

        # DicomExport only queries as STUDY level
        if ds.QueryRetrieveLevel == 'STUDY':
            if 'StudyInstanceUID' in ds:
                if ds.StudyInstanceUID not in ['*', '', '?']:
                    matching = [
                        inst for inst in instances if inst.StudyInstanceUID == ds.StudyInstanceUID
                    ]
                else:
                    matching = instances

            else:
                raise RuntimeError("Study query not requesting StudyInstanceUID")

        else:
            raise RuntimeError("Invalid query level {} for testing".format(ds.QueryRetrieveLevel))

        for instance in matching:
            # Check if C-CANCEL has been received
            if event.is_cancelled:
                yield 0xFE00, None
                return

            identifier = Dataset()

            # Copy all the queried attributes
            for attribute in ds:
                if attribute.tag in instance:
                    identifier[attribute.tag] = instance[attribute.tag]

            identifier.QueryRetrieveLevel = ds.QueryRetrieveLevel

            # Pending
            yield 0xFF00, identifier
        return
