import os
import unittest

from DicomExport.sql import Query


class TestStudyQuerySQL(unittest.TestCase):

    def setUp(self):
        host = os.getenv('DICOM_EXPORT_DB_HOST', 'postgres')
        self.querySCU = Query(host, 5432, 'pv_database_v1.0', user='nmr')

    def tearDown(self):
        self.querySCU.close()

    def test_query_for_studies(self):
        response = self.querySCU.query_studies()
        studies = [study.StudyInstanceUID for study in response]
        self.assertTrue('2.16.756.5.5.100.8323328.44748.1529342280.12' in studies)
        self.assertTrue('2.16.756.5.5.100.8323328.161533.1526396562.3' in studies)
        self.assertTrue('2.16.756.5.5.100.8323328.129754.1538486283.3' in studies)

    def test_query_for_study_details(self):
        study_uuid = '2.16.756.5.5.100.8323328.44748.1529342280.12'
        response = self.querySCU.query_studies(StudyInstanceUID=study_uuid, StudyDescription='')
        study_details = next(response)
        self.assertEqual(study_details.StudyDate, '20180618')
        self.assertEqual(study_details.StudyTime, '131800.532000')
        self.assertEqual(study_details.AccessionNumber, '')
        self.assertEqual(study_details.ReferringPhysicianName, '')
        self.assertEqual(study_details.StudyDescription, 'Bartha^CEST94T')
        self.assertEqual(study_details.PatientName, 'AgarPhantom^^^^')
        self.assertEqual(study_details.PatientID, 'Bartha^CEST94T_AgarPhantom')
        self.assertEqual(study_details.PatientBirthDate, '')
        self.assertEqual(study_details.PatientSex, 'O')
        self.assertEqual(study_details.StudyInstanceUID, '2.16.756.5.5.100.8323328.44748.1529342280.12')
        self.assertEqual(study_details.StudyID, '20180618_01')
        with self.assertRaises(StopIteration):
            next(response)


if __name__ == '__main__':
    unittest.main()
