# DicomExport

GUI for exporting non-image data to a DICOM server as DICOM series

## Console Deployment

1. Download the latest successful `master` branch build of `DicomExport` from [Appveyor](https://ci.appveyor.com/project/isolovey/dicomexport/history).

1. Move `DicomExport.zip` to console and unzip in `%medhome%\MriCustomer\install`

1. In the `config` subdirectory, pick a configuration `.yml` file with the correct settings.

1. Navigate to the output directory in `cmd` and run `install`. You'll be prompted for name of config file from previous step.

1. **IMPORTANT**: every commit deployed to a console should be marked with a git tag containing the console and installation date in the format `<console>-YYYMMDD`, e.g. `7T-20191216`.

## EDM installation

`DicomExport` depends on `EDM` python package manager to run properly on MR consoles. It needs to be installed once if EDM has not been previously installed.

1. Download and install Windows 7+ command-line-only version from [EDM website](https://www.enthought.com/enthought-deployment-manager/)
1. Install into `%medhome%\MriCustomer\CFMM\Enthought\edm`.
1. Do not add to path or install for all users.
1. Place the following `.edm.yaml` config file in the user's home directory (e.g. `C:\Users\cfmm\.edm.yaml` if user is `cfmm`) :

```yaml
# Change this if you are using on-premise brood
store_url: "https://packages.enthought.com"

repositories:
 - enthought/commercial
 - enthought/free

files_cache: "c:/MedCom/MriCustomer/CFMM/Enthought/cache"
root: "c:/MedCom/MriCustomer/CFMM/Enthought/.edm"
```

## Testing

One time only setup of a postgres container is required. Note the tag on the image, e.g. `master`, may require 
modificiation.

```sh
docker create -p 5432:5432  --name=dicomexport  registry.gitlab.com/cfmm/dicomexport/postgres:master
docker start dicomexport
psql -h localhost -p 5432 -d pv_database_v1.0 -U nmr < test/files/makedb.sql
psql -h localhost -p 5432 -d pv_database_v1.0 -U nmr < test/files/populate.sql
```

You can then start and stop as required. It needs to be running for SQL tests to be successful.

```
docker start dicomexport
docker stop dicomexport
```

You will need to define environment variable `DICOM_EXPORT_DB_HOST=localhost`, make `postgres` resolved to 
`127.0.0.1`, e.g. adding to `/etc/hosts`, or configure docker to allow direct host to container access.

