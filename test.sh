#!/usr/bin/env bash
set -x

platform=${1}

edm_path=`which edm`
if [[ $? == 1 ]]; then
  if [[ "$platform" == "win64" ]]; then
      edm_path="${HOME}/AppData/Local/Programs/Enthought/edm/edm.bat"
  else
      edm_path="$(pwd)/edm/bin/edm"
  fi
fi

${edm_path} environments remove -y DicomExportTest
${edm_path} environments import --force --filename DicomExport.databundle DicomExportTest

for p in $(ls -d eggs/*.egg); do
    ${edm_path} egginst --environment DicomExportTest $p;
done

${edm_path} run --environment DicomExportTest -- python -m unittest discover -v test/edm
