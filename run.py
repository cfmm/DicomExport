import os
import sys
import yaml

path = os.path.abspath(os.path.dirname(__file__))

for module in ('pynetdicom', 'DicomRaw', 'python-zipstream'):
    add_path = os.path.join(path, module)
    sys.path.insert(1, add_path)

from DicomExport.main import parsearg, AppendThread, main

if __name__ == '__main__':

    args = parsearg(path)
    config = yaml.load(open(args.config, 'r'), Loader=yaml.FullLoader)
    config['debug'] = args.debug
    if args.command == 'cli':
        append = AppendThread(args.input_directory, args.study_iuid)
        append.run()
    else:
        main(config)
