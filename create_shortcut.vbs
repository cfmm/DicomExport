if WScript.Arguments.Count < 6 then
    WScript.Echo "Missing params"
	Wscript.Quit()
end if

' Create the File System Object
Set objFSO = CreateObject("Scripting.FileSystemObject")

'Read params
strShortcutName = Wscript.Arguments(0)
strShortcutDir = Wscript.Arguments(1)
strPythonPath = Wscript.Arguments(2)
strMainPath = Wscript.Arguments(3)
strConfigFile = Wscript.Arguments(4)
strOtherParams = Wscript.Arguments(5)

'Check the paths
If (not objFSO.FolderExists(strShortcutDir)) Then
	Wscript.Echo "Invalid shortcut dir " & strShortcutDir
	Wscript.Quit()
End If
If (not objFSO.FileExists(strPythonPath)) Then
	Wscript.Echo "Invalid Python path " & strPythonPath
	Wscript.Quit()
End If
If (not objFSO.FileExists(strMainPath)) Then
	Wscript.Echo "Invalid main.py path " & strMainPath
	Wscript.Quit()
End If
If (not objFSO.FileExists(strConfigFile)) Then
	Wscript.Echo "Invalid config file path " & strConfigFile
	Wscript.Quit()
End If
' Get directory of main.py
Set objFile = objFSO.GetFile(strMainPath)
sName = objFile.Name
sPath = objFile.Path
strWorkingDir = Left(sPath, Len(sPath)-Len(sName))

strScriptDir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

Set objShell = WScript.CreateObject("WScript.Shell")
strShortcutPath = strShortcutDir & "\" & strShortcutName & ".lnk"
Wscript.Echo strShortcutPath
Set lnk = objShell.CreateShortcut(strShortcutPath)

lnk.TargetPath = strPythonPath
lnk.Arguments = strMainPath & " --config=" & strConfigFile & " " & strOtherParams
lnk.Description = strShortcutName
lnk.IconLocation = "C:\WINDOWS\system32\SHELL32.dll, 78"
lnk.WindowStyle = "1"
lnk.WorkingDirectory = strWorkingDir
lnk.Save

Wscript.Echo "Created link in " & strShortcutPath
Set lnk = Nothing