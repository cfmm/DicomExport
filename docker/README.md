To build the docker images and upload them to `registry.gitlab.com`

```bash
  docker compose build
  docker compose push
```
