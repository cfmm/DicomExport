from setuptools import setup, find_packages

setup(
      name='dicomexport',
      version='3.0.0',
      description='application for querying a DICOM server for studies and appending'
                  ' to them arbitrary files/directories as encapsulated DICOM series.',
      url='https://gitlab.com/cfmm/dicomexport',
      author='Igor Solovey',
      author_email='isolovey@robarts.ca',
      license='MIT',
      packages=find_packages(),
      zip_safe=False,
      install_requires=[
            'dicomraw>=3.0.0',
            'pydicom==1.2.2',
      ]
)
