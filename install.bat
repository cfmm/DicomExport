@echo off

set "MRI_CUSTOMER_BIN=%medhome%\MriCustomer\bin"
set "MRI_CUSTOMER_DEPLOY=%medhome%\MriCustomer\CFMM"

rem ensure deploy directory is not installation (i.e. current) directory
if /i "%MRI_CUSTOMER_DEPLOY%\DicomExport" == "%cd%" (
    echo Installation directory should be in %MRI_CUSTOMER_DEPLOY%\install. Not =%cd%.
    goto :eof
)

set DICOMEXPORT_CONFIG=3T-test

rem ask user for config file to use
set config_candidate=%DICOMEXPORT_CONFIG%
echo You need a valid configuration file for this system. See config subfolder.
set /p config_candidate=Enter configuration file name or [ENTER] for default value [%DICOMEXPORT_CONFIG%]:

rem strip file extension if present
for %%f in ("%config_candidate%") do set config_candidate=%%~nf

rem see if config file exists in the install directory and exit if it does not
set config_file="%cd%\config\%config_candidate%.yml"
if not exist "%config_file%" (
    echo "Invalid path %config_file%"
	goto :eof
) else (
	echo setting DICOMEXPORT_CONFIG to %config_file%
)
set DICOMEXPORT_CONFIG=%config_candidate%

@echo on
set EDM_EXECUTABLE="%medhome%\MriCustomer\CFMM\Enthought\edm\edm.bat"

rem install or re-install DicomExport EDM env
call %EDM_EXECUTABLE% environments remove -y DicomExport
call %EDM_EXECUTABLE% environments import --force --filename DicomExport.databundle DicomExport

rem install all the custom eggs into the EDM env
for /R eggs %%G IN (*.egg) do call %EDM_EXECUTABLE% egginst --environment DicomExport %%G

rem get DicomExport environment path from edm
for /f %%i in ('%EDM_EXECUTABLE% environments prefix DicomExport') do set DICOMEXPORT_PYTHON_DIR=%%i

rem copy all config files and main.py to deployment directory
call xcopy "%cd%\config" "%MRI_CUSTOMER_DEPLOY%\DicomExport\config" /i /y
echo f | call xcopy "%cd%\run.py" "%MRI_CUSTOMER_DEPLOY%\DicomExport\run.py" /i /y

rem create lnk shortcuts
call cscript "%cd%\create_shortcut.vbs"^
 "DicomExport"^
 "%MRI_CUSTOMER_BIN%"^
 "%DICOMEXPORT_PYTHON_DIR%\pythonw.exe"^
 "%MRI_CUSTOMER_DEPLOY%\DicomExport\run.py"^
 "%MRI_CUSTOMER_DEPLOY%\DicomExport\config\%DICOMEXPORT_CONFIG%.yml"^
 "gui"

call cscript "%cd%\create_shortcut.vbs"^
 "DicomExport (Debug)"^
 "%MRI_CUSTOMER_BIN%"^
 "%DICOMEXPORT_PYTHON_DIR%\pythonw.exe"^
 "%MRI_CUSTOMER_DEPLOY%\DicomExport\run.py"^
 "%MRI_CUSTOMER_DEPLOY%\DicomExport\config\%DICOMEXPORT_CONFIG%.yml"^
 "--debug gui"
