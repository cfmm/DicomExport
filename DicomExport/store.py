
from pynetdicom import StoragePresentationContexts
from pydicom.uid import ImplicitVRLittleEndian, ExplicitVRBigEndian, ExplicitVRLittleEndian

from .scu import SCU
from DicomExport import logger

RawImageSOPClassUID = '1.2.840.10008.5.1.4.1.1.66'
RawImagePresentationContext = [x for x in StoragePresentationContexts if x.abstract_syntax == RawImageSOPClassUID]


class Store(SCU):
    """Store SCU handles streaming C-STORE."""
    def __init__(self, remote_host, remote_port, remote_ae_title, local_ae_title='dicomwrapper'):
        super(Store, self).__init__(remote_host, remote_port, remote_ae_title,
                                    presentation_contexts=RawImagePresentationContext,
                                    local_ae_title=local_ae_title)

    def send(self, input_stream, sop_instance_uid, is_implicit_vr, is_little_endian):
        """Open a new association and send stream."""

        ivr = is_implicit_vr
        le = is_little_endian
        transfer_syntax = le*(ImplicitVRLittleEndian*ivr + ExplicitVRLittleEndian*(not ivr))\
            + (not le)*(not ivr)*ExplicitVRBigEndian or None

        if self.association:
            logger.debug("Store.send: closing an existing association")
            self.close()
        logger.debug("Store.send: opening an association")
        self._open()
        try:
            logger.debug("Store.send: send streaming C-STORE.")

            self.association.send_c_store(
                input_stream,
                streaming=True,
                sop_instance_uid=sop_instance_uid,
                abstract_syntax=RawImagePresentationContext[0].abstract_syntax,
                transfer_syntax=transfer_syntax
            )
        except (RuntimeError, AttributeError) as e:
            logger.error(e.message)
            raise
