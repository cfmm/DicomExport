from pydicom.dataset import Dataset
from pynetdicom import QueryRetrievePresentationContexts
from .scu import SCU


class Query(SCU):
    """Handles Query Retrieve SOP queries"""
    def __init__(self, remote_host, remote_port, remote_ae_title, local_ae_title='dicomwrapper'):
        super(Query, self).__init__(remote_host, remote_port, remote_ae_title,
                                    presentation_contexts=QueryRetrievePresentationContexts,
                                    local_ae_title=local_ae_title)

    def _set_required_query_attributes(self, dataset, query_model, query_level):
        """Set required query attributes to empty for each query model/level."""
        # Only a subset which is needed is defined, NotImplementedError raised for the rest. See PS3.4 C.3 for details.
        if query_model == 'S':
            if query_level == 'STUDY':
                dataset.PatientID = ''
                dataset.PatientName = ''
                dataset.StudyDate = ''
                dataset.StudyTime = ''
                dataset.AccessionNumber = ''
                dataset.StudyID = ''
                dataset.StudyDescription = ''
                # mandatory unique key
                dataset.StudyInstanceUID = ''
            elif query_level == 'SERIES':
                dataset.Modality = ''
                dataset.SeriesNumber = ''
                # mandatory unique key
                dataset.SeriesInstanceUID = ''
            elif query_level == 'IMAGE':
                dataset.InstanceNumber = ''
                # mandatory unique key
                dataset.SOPInstanceUID = ''
            else:
                raise NotImplementedError
        else:
            raise NotImplementedError

        if hasattr(dataset, 'StudyDate'):
            # exclude studies without a study date
            dataset.StudyDate = '19000101-'

        dataset.QueryRetrieveLevel = query_level

        return dataset

    def query(self, query_model, query_level, **query_params):
        """Perform a query after setting required query attributes"""
        dataset = Dataset()
        # defaults
        dataset = self._set_required_query_attributes(dataset, query_model, query_level)
        # passed attributes
        for key in query_params:
            setattr(dataset, key, query_params[key])

        if self.association:
            self.close()
        self._open()
        return self.association.send_c_find(dataset, query_model=query_model)

    def query_studies(self, **query_params):
        """Query for studies"""
        return self._query_result_iterator('S', 'STUDY', **query_params)

    def query_series(self, **query_params):
        """Query for series"""
        return self._query_result_iterator('S', 'SERIES', **query_params)

    def query_instances(self, **query_params):
        """Query for instances/images"""
        return self._query_result_iterator('S', 'IMAGE', **query_params)

    def _query_result_iterator(self, query_model, query_level, **query_params):
        """Query results iterator"""
        query_results = self.query(query_model, query_level, **query_params)
        for result in query_results:
            if result[0].Status == 0xff00:
                yield result[1]
            elif result[0].Status == 0x0000:
                break
            elif result[0].Status == 0xC311:
                break
            else:
                raise RuntimeError('Unknown code')
