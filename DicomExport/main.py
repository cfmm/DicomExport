#!/usr/bin/env python
import argparse
import os
import socket

from datetime import datetime

from time import sleep

import yaml
from pydicom.uid import UID, generate_uid

from DicomExport import Query, SQLQuery, Store, DicomCommunicationError
from DicomExport import RawDicomDataset, logger
from DicomExport.gui import GUI, messagebox
import threading
import traceback


class Study(object):
    """Wrapper for a Study Dataset"""
    def __init__(self, dataset):
        self._dataset = dataset

    def __getattr__(self, name):
        return getattr(self._dataset, name)

    def __str__(self):
        """Return constant-length string with tabulated description, date and time of study"""
        dt = self.as_datetime()
        return "{:<50}{:<50}{:<20}{:<20}".format(
            self.StudyDescription or self.PatientID,
            str(self.PatientName.lstrip("^")),
            dt.strftime("%d-%b-%Y"),
            dt.strftime("%H:%M:%S")
        )

    def as_datetime(self):
        return datetime.strptime(self.StudyDate+self.StudyTime[:6], '%Y%m%d%H%M%S')


class StudyIterable(object):
    def __init__(self, iterable):
        self._iterable = iterable

    def __iter__(self):
        for item in sorted(self._iterable, key=lambda study: study.as_datetime(), reverse=True):
            yield item


def get_scu(data, aetitle, base=None):
    if data.get("type", "dicom") == "postgresql":
        scu = SQLQuery(
            remote_host=data["host"],
            remote_port=data["port"],
            database=data["database"],
        )
    else:
        if base is None:
            base = Query

        scu = base(
            remote_host=data["host"],
            remote_port=data["port"],
            remote_ae_title=data["ae_title"],
            local_ae_title=aetitle
        )
    return scu


class AppendThread(threading.Thread):
    """Thread which handles the dicom raw append"""
    class AppendThreadStopExecution(threading.ThreadError):
        pass

    def __init__(self, directory_or_file, study_instance_uid, series_description, config_, progress_callback=None, finished_callback=None):
        """
        :param directory_or_file: path to valid directory or file which will be compressed and appended to study
        :param study_instance_uid: IUID to append to
        :param series_description: a description used for dicom Series Description attribute and encapsulated archive name
        :param config_: config dictionary
        :param progress_callback: to call when progress is to be updated
        :param finished_callback: to call when execution is finished
        """
        super(AppendThread, self).__init__(name="append_thread")
        self._directory_or_file = directory_or_file
        self._study_instance_uid = study_instance_uid
        self._series_description = series_description
        self._progress_callback = progress_callback
        self._finished_callback = finished_callback
        self._stop = threading.Event()  # This will be used to abort thread execution on demand
        self.query = get_scu(config_["query_server"], config_["client_ae_title"])
        self.store = get_scu(config_["store_server"], config_["client_ae_title"], Store)
        self._raw_append = RawDicomDataset(query=self.query, store=self.store)

    def stop(self):
        self._stop.set()

    def _internal_callback(self, value):
        """Call progress callback after checking for stop condition"""
        if self._stop.isSet():
            logger.debug("AppendThread internal callback: Detected stop condition. Throwing AppendThreadStopExecution.")
            raise AppendThread.AppendThreadStopExecution
        if self._progress_callback:
            self._progress_callback(value)

    def run(self):
        logger.info("Starting Append...")
        try:
            logger.debug("Calling raw_append.append")
            # generate unique study and sop instance UIDs from the current timestamp
            timestamp = datetime.now().strftime('%d%b%Y%H%M%S%f')
            self._raw_append.append(
                self._directory_or_file,
                self._study_instance_uid,
                series_description=self._series_description,
                progress_callback=self._internal_callback,
                sop_instance_uid=generate_uid(entropy_srcs=[
                    'sop_instance_uid',
                    timestamp
                ]),
                series_instance_uid=generate_uid(entropy_srcs=[
                    'series_instance_uid',
                    timestamp
                ]),
            )
            logger.success("Append is successful.")
        except AppendThread.AppendThreadStopExecution:
            logger.debug("AppendThread run(): detected AppendThreadStopExecution")
        except:
            logger.debug("AppendThread run(): detected an error")
            logger.error("Append FAILED.", exc_info=True)
        if self._finished_callback:
            self._finished_callback()
        logger.debug("Append thread is done.")
        self.query.close()
        self.store.close()


def parsearg(path=''):
    """
    Parse input arguments to application.

    :return: options
    """

    def readable_dir_or_file(item):
        try:
            item = os.path.abspath(item)
            assert os.path.isdir(item) or os.path.isfile(item)
        except:
            raise argparse.ArgumentTypeError("readable_dir_or_file:{0} is not a valid path".format(item))
        if os.access(item, os.R_OK):
            return item
        else:
            raise argparse.ArgumentTypeError("readable_dir_or_file:{0} is not a readable dir or file".format(item))

    def valid_uid(item):
        uid = UID(item)
        if not uid.is_valid():
            raise argparse.ArgumentTypeError("{0} is not a valid UID".format(item))
        return item

    parser = argparse.ArgumentParser(description='DicomExport')

    parser.add_argument('--debug', '-d',
                        dest='debug',
                        action='store_true',
                        default=False,
                        help='Launch in debug mode')
    subparser = parser.add_subparsers(dest="command")
    cli_parser = subparser.add_parser('cli', help="Run in CLI mode. Must specify study instance UID, source directory")
    cli_parser.add_argument('--input-path', '-i', type=readable_dir_or_file, required=True, help="Input directory or file")
    cli_parser.add_argument('--study_iuid', '-s', type=valid_uid, required=True)
    cli_parser.add_argument('--series_description',
                            help="Series description to use in archive and in Dicom Series Description attribute. "
                                 "If blank, will be derived from input-path.")
    subparser.add_parser('gui', help="Run in GUI mode.")

    hostname = socket.gethostname()
    default_config = os.path.join(path, hostname + '.yml')

    parser.add_argument('--config', '-C',
                        dest='config',
                        action='store',
                        default=default_config,
                        help='Path to config file')

    args = parser.parse_args()
    if args.command=='cli' and args.series_description is None:
        args.series_description = os.path.splitext(os.path.basename(args.input_path.rstrip(os.sep)))[0]
    return args


def main(config_):
    # query object to obtain list of all studies on the query server
    try:
        query = get_scu(config_['query_server'], config_['client_ae_title'])
        app = GUI(
            studies=StudyIterable((Study(s) for s in query.query_studies(StudyDescription=''))),
            debug=config_['debug'],
            config=config_
        )
    except DicomCommunicationError:
        messagebox.showerror(
            title='Dicom Communication Error',
            message='Error establishing a connection with the query server',
            detail=traceback.format_exc()
        )

    def main_callback(study, directory_or_file, description, progress_callback, finished_callback):
        """
        :param study: study to append to (as dataset)
        :param directory_or_file: path to valid directory or file which will be compressed and appended to study
        :param description: used in dicom Series Description attribute and archive name
        :param progress_callback: to call when progress is to be updated
        :param finished_callback: to call when execution is finished
        :return:
        """
        logger.debug("main_callback: Initializing thread")
        append_thread = AppendThread(
            directory_or_file, study.StudyInstanceUID, description, config_, progress_callback, finished_callback
        )
        sleep(0.5)
        append_thread.start()
        app.append_thread = append_thread

    app.callback = main_callback
    app.mainloop()


if __name__ == '__main__':

    args = parsearg()
    config = yaml.load(open(args.config, 'r'))
    config['debug'] = args.debug
    if args.command == 'cli':
        append = AppendThread(args.input_directory, args.study_iuid, args.series_description, config)
        append.run()
    else:
        main(config)
