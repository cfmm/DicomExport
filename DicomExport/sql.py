import os
import re
from pydicom.dataset import Dataset


class Query(object):
    def __init__(self, remote_host, remote_port, database, **db_args):
        """
        Initialize a postgresql connection to database on specified host and port
        :param remote_host:
        :param remote_port:
        :param database:
        """

        # Bruker uses a dynamic port, so find the socket in /tmp
        if remote_host.startswith('/'):
            socket = next(f for f in os.listdir(remote_host) if re.match(remote_port, f))
            remote_port = int(socket.rsplit('.', 1)[-1])
        import psycopg2
        self.conn = psycopg2.connect(host=remote_host, port=remote_port, database=database, **db_args)

    def query_studies(self, **query_params):
        """Query for studies"""
        uuid = query_params.pop('StudyInstanceUID', None)

        sql = ("""
        SELECT t3.registration AS patientid, t3.displayname AS patientname,
                 t1.displayname AS studyid, t1.studydate AS studydate, t1.registration AS studyinstanceuid,
                 COALESCE(t4.displayname,
                          SUBSTRING(t3.registration, CONCAT('[^^]+?\\^.+?(?=_', t3.displayname, '|\\^)')),
                          t3.registration) AS studydescription,
                 t3.gender AS patientsex, t3.dateofdelivery AS patientbirthdate
            FROM studyentity AS t1
            INNER JOIN sessionentity AS t2 ON t1.sessionentity_id=t2.id
            INNER JOIN (SELECT id, registration, displayname, gender, dateofdelivery FROM animalsubjectentity
                        UNION SELECT id, registration, displayname,
                                'UNKNOWN' AS gender, NULL AS dateofdelivery FROM materialsubjectentity
                        UNION SELECT id, registration, displayname,
                                'UNKNOWN' AS gender, dateofdelivery FROM plantsubjectentity
                        UNION SELECT id, registration, displayname,
                                'UNKNOWN' AS gender, dateofdelivery FROM fungussubjectentity) AS t3
                        ON t2.subjectentity_id=t3.id
            LEFT OUTER JOIN projectentity t4 ON t2.projectentity_id=t4.id
        """)

        if uuid:
            sql += " WHERE t1.registration = '{}'".format(uuid)

        cur = self.conn.cursor()
        cur.execute(sql)

        row = cur.fetchone()
        while row is not None:
            dataset = Dataset()
            dataset.PatientID = row[0]
            dataset.PatientName = row[1] + "^^^^"
            dataset.StudyID = row[2]
            dataset.StudyDate = row[3].strftime('%Y%m%d')
            dataset.StudyTime = row[3].strftime('%H%M%S.%f')
            dataset.StudyInstanceUID = row[4]
            dataset.StudyDescription = row[5]

            if row[6] == 'MALE':
                dataset.PatientSex = 'M'
            elif row[6] == 'FEMALE':
                dataset.PatientSex = 'F'
            else:
                dataset.PatientSex = 'O'

            if row[7]:
                dataset.PatientBirthDate = row[7].strftime('%Y%m%d')
            else:
                dataset.PatientBirthDate = ''

            dataset.AccessionNumber = ''
            dataset.ReferringPhysicianName = ''

            yield dataset
            row = cur.fetchone()
        cur.close()

    def close(self):
        pass
