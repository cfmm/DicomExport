import logging as _logging

logger = _logging.getLogger("DicomExport")
_logging.basicConfig(format="%(asctime)s: (%(levelname)s) %(message)s")
# define a custom success level
_SUCCESS = 21  # > INFO(=20) to make sure it is always displayed
_logging.addLevelName(_SUCCESS, "SUCCESS")


def _success(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(_SUCCESS):
        self._log(_SUCCESS, message, args, **kws)


_logging.Logger.success = _success
logger.setLevel("DEBUG")

from .scu import DicomCommunicationError
from .query import Query
from .store import Store
from .raw import RawDicomDataset
from .sql import Query as SQLQuery
