import sys
import os
import time
import logging
import queue as queue

from DicomExport import logger

if sys.version_info[0] < 3:
    import Tkinter as tk
    import tkFileDialog as tkFileDialog
    import ttk
    import tkMessageBox as messagebox
    import tkFont
else:
    import tkinter as tk
    import tkinter.filedialog as tkFileDialog
    import tkinter.ttk as ttk
    from tkinter import messagebox
    from tkinter import font as tkFont


def get_size(path):
    total_size = 0
    if type(path) is not list and type(path) is not tuple:
        path = [path]
    for p in path:
        if os.path.isfile(p):
            total_size += os.path.getsize(p)
        elif os.path.isdir(p):
            for dirpath, dirnames, filenames in os.walk(p):
                for filename in filenames:
                    filepath = os.path.join(dirpath, filename)
                    total_size += os.path.getsize(filepath)
        else:
            raise RuntimeError("{0} is not a valid file or directory".format(p))
    return total_size


def get_description(path):
    if type(path) is not list and type(path) is not tuple:
        p = path
    else:
        p = path[0]
    return os.path.splitext(os.path.basename(p.rstrip(os.sep)))[0]


def sizeof_fmt(num, suffix='B'):
    """https://stackoverflow.com/a/1094933/1168152"""
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


class SelectionPopup(tk.Toplevel):
    """Popup window for informing user of dataset size and displaying an editable default series description

    When the selection is made, the object contains two TkInter vars:
     self.description - the chosen description
     self.decision - True if OK button is clicked.
    """
    def __init__(self, size, default_description, master=None):
        tk.Toplevel.__init__(self, master=master)
        self.grid()
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=0)
        row = 0
        self.info = tk.Label(self,
                             text='Append directory or file(s) as raw DICOM:',
                             justify=tk.LEFT,
                             )
        self.info.grid(row=row, column=0, columnspan=2, padx=5, pady=5)
        row += 1

        self.size_label = tk.Label(self, text='Size:', justify=tk.RIGHT)
        self.size_label.grid(row=row, column=0, padx=5, pady=5)
        self.size = tk.Label(self, text=size, justify=tk.RIGHT)
        self.size.grid(row=row, column=1, padx=5, pady=5)
        row += 1

        self.description_label = tk.Label(self, text='Series Description:', justify=tk.RIGHT)
        self.description_label.grid(row=row, column=0, padx=5, pady=5)
        self.description = tk.StringVar()
        self.description.set(default_description)
        self.description_entry = tk.Entry(self, textvariable=self.description, justify=tk.LEFT)
        self.description_entry.grid(row=row, column=1, padx=5, pady=5)
        row += 1

        self.decision = tk.BooleanVar()
        self.ok_button = tk.Button(self, text="OK", padx=5, command=lambda: self._set_decision_and_quit(True))
        self.ok_button.grid(row=row, column=0, padx=5, pady=5)
        self.cancel_button = tk.Button(self, text="Cancel", padx=5, command=lambda: self._set_decision_and_quit(False))
        self.cancel_button.grid(row=row, column=1, padx=5, pady=5)

        # Center the window
        self.update_idletasks()
        w = self.winfo_screenwidth()
        h = self.winfo_screenheight()
        size = tuple(int(_) for _ in self.geometry().split('+')[0].split('x'))
        x = w / 2 - size[0] / 2
        y = h / 2 - size[1] / 2
        self.geometry("%dx%d+%d+%d" % (size + (x, y)))

    def _set_decision_and_quit(self, decision):
        self.decision.set(decision)
        self.destroy()


class GUI(tk.Frame):
    """Tkinter GUI for DicomExport"""

    class ThreadSafeConsole(tk.Text):
        """
        Uses a queue to implement a thread-safe text console.
        see http://effbot.org/zone/tkinter-threads.htm
        """
        def __init__(self, master, **options):
            tk.Text.__init__(self, master, **options)
            self.queue = queue.Queue()
            self.update_me()

        def write(self, text, levelname):
            self.queue.put((text, levelname))

        def clear(self):
            self.queue.put(None)

        def update_me(self):
            try:
                while 1:
                    line = self.queue.get_nowait()
                    if line is None:
                        self.delete(1.0, tk.END)
                    else:
                        self.config(state='normal')
                        self.insert(tk.END, str(line[0]), line[1])
                        # If error or critical, make sure debug textarea is visible
                        if line[1] == "ERROR" or line[1] == "CRITICAL":
                            # show text widget which displays the error
                            if not self.master.debug_textarea_visible:
                                self.master.toggle_debug_textarea(True)
                    self.see(tk.END)
                    self.master.update_idletasks()
                    self.config(state='disabled')
            except queue.Empty:
                pass
            self.after(300, self.update_me)

    class Handler(logging.Handler):
        """ logging handler that prints to a text widget"""

        def __init__(self, widget, level):
            logging.Handler.__init__(self)
            self.setLevel(level)
            self.setFormatter(logging.Formatter("%(asctime)s: (%(levelname)s) %(message)s"))
            self.widget = widget
            self.widget.config(state='disabled')  # cannot write in text area
            self.widget.tag_config("INFO", foreground="blue")
            self.widget.tag_config("DEBUG", foreground="grey")
            self.widget.tag_config("WARNING", foreground="orange")
            self.widget.tag_config("ERROR", foreground="red")
            self.widget.tag_config("SUCCESS", foreground="green")
            self.widget.tag_config("CRITICAL", foreground="red", underline=1)

        def emit(self, record):
            """Insert a log message into the text area."""
            self.widget.write(self.format(record) + '\n', record.levelname)

    def __init__(self, studies, debug=False, config=None, callback=None, master=None):
        tk.Frame.__init__(self, master=master)
        self.master.title('DICOM Export')

        self.grid()
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=0)
        self.callback = callback
        if config is None:
            config = {}
        self.config = config

        row = 0
        self.study_label = tk.Label(self,
                                    text='Select existing DICOM study to append to',
                                    justify=tk.LEFT,
                                    )

        self.study_label.grid(row=row,
                              column=0,
                              columnspan=3,
                              padx=5,
                              pady=5,
                              sticky=tk.W,
                              )
        row += 1

        # Create a study Listbox (with Scrollbar) with all the available studies to select from
        self.study_scrollbar = tk.Scrollbar(self)
        self.study_scrollbar.grid(row=row, column=2, sticky=(tk.N, tk.S, tk.W))
        self.study_list = tk.Listbox(self,
                                     selectmode=tk.SINGLE,
                                     width=140,
                                     yscrollcommand=self.study_scrollbar.set
                                     )
        fixed_font = tkFont.nametofont('TkFixedFont')
        if 'font_size' in self.config:
            fixed_font.configure(size=self.config['font_size'])
        self.study_list.config(font=fixed_font)
        self.study_scrollbar.config(command=self.study_list.yview)
        self.study_list.insert(tk.END, " " * 90)

        self.study_list.grid(row=row,
                             column=0,
                             columnspan=2,
                             padx=5,
                             sticky=tk.W,
                             )

        row += 1

        # Create a send progress bar
        self.progress = ttk.Progressbar(self,
                                        orient='horizontal',
                                        mode='determinate',
                                        )
        self.progress.grid(row=row,
                           column=0,
                           columnspan=3,
                           padx=5,
                           pady=5,
                           sticky=tk.E + tk.W,
                           )

        row += 1

        # Create button to select the directory for export
        self.directory_button = tk.Button(self,
                                          text='Select Directory to Append',
                                          command=self.get_directory,
                                          )
        # Create button to select the file for export
        self.file_button = tk.Button(self,
                                     text='Select File(s) to Append',
                                     command=self.get_file,
                                     )

        # Create a button to quit without exporting anything
        self.exit_button = tk.Button(self,
                                     text='Exit',
                                     command=self.on_closing,
                                     )

        self.directory_button.grid(row=row,
                                   column=0,
                                   padx=5,
                                   pady=5,
                                   ipadx=5,
                                   ipady=2,
                                   sticky=tk.E,
                                   )
        self.file_button.grid(row=row,
                              column=1,
                              padx=5,
                              pady=5,
                              ipadx=5,
                              ipady=2,
                              sticky=tk.E,
                              )
        self.exit_button.grid(row=row,
                              column=2,
                              padx=5,
                              pady=5,
                              ipadx=5,
                              ipady=2,
                              sticky=tk.E,
                              )
        # create button to show details (i.e. the debug/log print area)
        self.details_button = tk.Button(self,
                                        text='Show Details',
                                        command=self.toggle_debug_textarea
                                        )
        self.details_button.grid(row=row,
                                 column=0,
                                 padx=5,
                                 pady=5,
                                 ipadx=5,
                                 ipady=2,
                                 sticky=tk.W,
                                 )

        row += 1

        # define details (log) textarea (with scrollbar)
        self.details_scrollbar = tk.Scrollbar(self)
        self.details_scrollbar.grid(row=row, column=1, sticky=(tk.N, tk.S, tk.W))

        self.details_area = GUI.ThreadSafeConsole(self, yscrollcommand=self.details_scrollbar.set)
        self.details_area.grid(row=row, column=0, sticky=(tk.N, tk.S, tk.E, tk.W))

        self.details_area.insert(tk.END, "Ready." + '\n', "INFO")

        self.details_scrollbar.config(command=self.details_area.yview)

        # initialize the logging handler. Also output pynetdicom log messages.
        if debug:
            level = logging.DEBUG
        else:
            level = logging.INFO
        self.logging_handler = GUI.Handler(self.details_area, level)
        logger.addHandler(self.logging_handler)
        logging.getLogger("pynetdicom").addHandler(self.logging_handler)

        self.center_window()
        self.debug_textarea_visible = False
        self.toggle_debug_textarea(debug)

        # associate self.on_closing with window closing
        self.master.protocol("WM_DELETE_WINDOW", self.on_closing)

        # this property is used to refer to the currently running append thread
        self.append_thread = None

        self.update_idletasks()
        self.studies = []
        for study in studies:
            self.study_list.insert(tk.END, str(study))
            self.studies.append(study)
        self.study_list.delete(0)

        # Automatically select the first study
        self.study_list.activate(0)
        self.study_list.selection_set(0, 0)

    def toggle_debug_textarea(self, status=None):
        """Control debug (log) text area visibility"""
        # if no status passed, toggle.
        if status is None:
            status = not self.debug_textarea_visible
        if status:
            self.details_area.grid()
            self.details_scrollbar.grid()
        else:
            self.details_scrollbar.grid_remove()
            self.details_area.grid_remove()
        self.debug_textarea_visible = status
        self.master.update_idletasks()
        # set geometry to the new window width/height, and existing x,y location
        self.master.geometry(
            '{0}x{1}+{2}+{3}'.format(
                self.winfo_width(),
                self.winfo_height(),
                self.master.winfo_x(),
                self.master.winfo_y()
            )
        )

    def on_closing(self):
        """Called when the window is closed (exit button or window close)"""

        # ask for quit confirmation if there's an append_thread active
        if self.append_thread is None or \
                messagebox.askokcancel("Quit",
                                       "Are you sure you want to quit? Any running append tasks will be terminated."):
            if self.append_thread:
                # emit the stop signal and wait for the thread to join
                self.append_thread.stop()
                self.append_thread.join()
                self.append_thread = None
            self.master.destroy()

    def center_window(self):
        # Center the window
        w = self.winfo_screenwidth()
        h = self.winfo_screenheight()
        self.master.update_idletasks()
        size = tuple(int(_) for _ in self.master.geometry().split('+')[0].split('x'))
        x = int(w / 2 - size[0] / 2)
        y = int(h / 2 - size[1] / 2)
        self.master.geometry("%dx%d+%d+%d" % (size + (x, y)))

    def _append_item(self, item, getsize):
        # Check that cancel was not selected
        if item:
            self.update_progress(0)
            if len(self.study_list.curselection()) == 0:
                messagebox.showerror("Append error",
                                     "Must select a study to append to.")
                return
            selected_study_index = int(self.study_list.curselection()[0])
            item_size = getsize(item)
            if 'size_limit' in self.config and item_size > self.config['size_limit']:
                messagebox.showerror("Append error",
                                     "Item size exceeds maximum ({0}>{1})".format(
                                         sizeof_fmt(item_size),
                                         sizeof_fmt(self.config['size_limit'])
                                     ))
                return

            item_description = get_description(item)
            self.selection_popup = SelectionPopup(
                size=sizeof_fmt(item_size),
                default_description=item_description,
                master=self)
            self.master.wait_window(self.selection_popup)
            self.study_list.activate(selected_study_index)
            self.study_list.selection_set(selected_study_index, selected_study_index)

            if self.selection_popup.decision.get():
                self.directory_button.config(state=tk.DISABLED)
                self.file_button.config(state=tk.DISABLED)
                self.update_progress(0.0)
                self.progress['maximum'] = 100
                logger.debug("Directory selected. Running callback.")
                self.callback(
                    self.studies[selected_study_index],
                    item,
                    self.selection_popup.description.get(),
                    self.update_progress,
                    self.append_finished
                )

    def get_file(self):
        file_opt = {
            'initialdir': os.getcwd(),
            'parent': self.master,
            'title': 'File to export'
        }
        filenames = tkFileDialog.askopenfilenames(**file_opt)
        filenames = self.master.tk.splitlist(filenames)  # needed on Windows, does nothing on OS X
        self._append_item(filenames, get_size)

    def get_directory(self):
        dir_opt = {
            'initialdir': os.getcwd(),
            'mustexist': True,
            'parent': self.master,
            'title': 'Directory to export'
        }
        self._append_item(
            tkFileDialog.askdirectory(**dir_opt),
            get_size
        )

    def update_progress(self, value):
        self.progress["value"] = value
        self.master.update_idletasks()

    def append_finished(self):
        self.append_thread = None
        # re-enable the directory and file select buttons
        self.directory_button.config(state=tk.NORMAL)
        self.file_button.config(state=tk.NORMAL)


if __name__ == '__main__':
    def test_callback(study, directory, progress_callback, _):
        print(study)
        print(directory)

        for x in range(0, 101, 10):
            progress_callback(x)
            time.sleep(0.5)


    app = GUI(studies=['test 1 abcdefghijklmnopqrstuvwxyz',
                       'test 3',
                       'test 2',
                       'test 4'],
              callback=test_callback)
    app.mainloop()
