from pydicom.uid import ExplicitVRLittleEndian
from pynetdicom import AE, VerificationPresentationContexts
from pynetdicom.utils import validate_ae_title
from DicomExport import logger


class DicomCommunicationError(IOError):
    pass


class SCU(object):
    """SCU object. Handles setting up AE and opening/closing associations"""

    def __init__(self, remote_host, remote_port, remote_ae_title, presentation_contexts, local_ae_title='dicomwrapper'):
        logger.debug("SCU.__init__: initializing AE title")

        # define an AE title for verification (C-ECHO) and requested SCU SOP Classes
        self.ae = AE(ae_title=local_ae_title)

        transfer_syntax = [ExplicitVRLittleEndian]

        # Add presentation contexts with specified transfer syntaxes
        for context in VerificationPresentationContexts:
            self.ae.add_requested_context(context.abstract_syntax, transfer_syntax)
        for context in presentation_contexts:
            self.ae.add_requested_context(context.abstract_syntax, transfer_syntax)

        logger.debug("SCU.__init__: initialized AE title. Setting timeouts.")
        self.ae.acse_timeout = 20
        self.ae.dimse_timeout = 20
        self.ae.network_timeout = 20
        logger.debug("SCU.__init__: set timeouts.")

        self.remote_host = remote_host
        self.remote_port = remote_port
        self.remote_ae_title = validate_ae_title(remote_ae_title)
        self.association = None
        logger.debug("SCU.__init__: done.")

    def __del__(self):
        # clean up AE, associations on delete
        self.close()
        self.ae.shutdown()

    def _identity(self):
        """Print local SCU and remote SCP details."""
        return "(" + self.__class__.__name__ + " SCU {0}@localhost) (remote SCP: {1}@{2}:{3}): ".format(
            self.ae.ae_title.decode('ascii').rstrip(),
            self.remote_ae_title.decode('ascii').rstrip(),
            self.remote_host,
            self.remote_port
        )

    def _open(self):
        """
        Open an association with the specified server
        """
        # Request association with remote
        self.association = self.ae.associate(
            addr=self.remote_host,
            port=self.remote_port,
            ae_title=self.remote_ae_title
        )

        if not self.association.is_established:
            msg = self._identity() + 'Cannot establish association with remote server.'
            logger.error(msg)
            raise DicomCommunicationError(msg)
        status = self.association.send_c_echo()
        if status.Status != 0x0000:
            msg = self._identity() + 'Cannot successfully send C-Echo to remote server.'
            logger.error(msg)
            raise DicomCommunicationError(msg)
        logger.info(self._identity() + "Established an association with server.")

    def close(self):
        """
        Close an open association
        """
        if self.association:
            self.association.release()
            logger.info(self._identity() + "Released an association with server.")
            self.association = None
